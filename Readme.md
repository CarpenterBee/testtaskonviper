# Отель 

## Описание

* Приложения разработанно в рамках выполнения тестового задания
* ТЗ: https://docs.google.com/document/d/1LFnyjkn1klQnyPl7dXju6VDhACbcqkuA0AvUoCh0yoM/edit
* Платформа: IOS

## Скриншоты

<img src="Screenshots/hotelScreen.png" width="197" height="426">
<img src="Screenshots/roomsScreen.png" width="197" height="426">
<img src="Screenshots/reservationScreenOpen.png" width="197" height="426">
<img src="Screenshots/reservationScreenClosed.png" width="197" height="426">
<img src="Screenshots/paymentScreen.png" width="197" height="426">

## Технологии

* Кастомные + переиспользуемые UI компоненты на UIKit 
* Верстка - code + NSLayoutConstraints
* Архитектура - VIPER
* UITableView + dynamic cell height
* UICollectionView - BrickCollection + UIFlowLayout, UserInfoCollection + dynamic height (на PerformBatchUpdate)
* Анимации - UIView.animate (HotelTextfield, UserInfoCollectionViewCell)
* Кастомный UIFont - SFProDisplay
* Кастомный сетевой слой (Generics) - URLcomponents, URLsession, Result<T, Error>
* Паттерны: Singleton, Builder ,Coordinator

### Устройства

* IPhone
* IOS 17.0

### Установка

* загрузить проект из GitLab

### Среда разработки

* реализованно при помощи XCode
* версия 15.2

### Инструкция по использованию

* На экране отеля указана необходимая информация об отеле. Можно перейти к выбору номера при нажатии кнопки внизу экрана.
* На экране выбора номера представлен список номеров с информацией о номере. Можно перейти к бронированию конкретного номера по кнопке.
* На экране бронирования пользователь может оставить свои личные данные и добавить необходимое количество гостей. Можно перейти к экрану оплаты бронирования.
* Если введенные данные пользователя некорректны, то поля для ввода подсветятся красным и не дадут оплатить бронирование.
* На экране завершения оплаты получаем информации об успешном бронировании с рандомным номером бронирования. Можно вернуться на экран бронирования или на главный экран по кнопке внизу экрана

 
## Разработчик

* Роман Степанов
* Почта - roma-s01@mail.ru
* Телеграм - @chester_hard

## История верий

* 1.0
