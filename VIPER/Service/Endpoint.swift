//
//  Endpoint.swift
//  VIPER
//
//  Created by Роман Степанов on 05.01.2024.
//

import Foundation

public enum Endpoint {
    
    case hotel(url: String = "/v3/d144777c-a67f-4e35-867a-cacc3b827473")
    case rooms(url: String = "/v3/8b532701-709e-4194-a41c-1a903af00195")
    case reservation(url: String = "/v3/63866c74-d593-432c-af8e-f279d1a8d2ff")
    
    public var request: URLRequest? {
        guard let url = self.url else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = self.httpMethod
        return request
    }
    
    private var url: URL? {
        var components = URLComponents()
        components.scheme = Constants.scheme
        components.host = Constants.baseURL
        components.path = self.path
        
        return components.url
    }
    
    private var path: String {
        switch self {
        case .hotel(let url): return url
        case .reservation(let url): return url
        case .rooms(let url): return url
        }
    }
    
    private var httpMethod: String {
        switch self {
        case.hotel,
                .reservation,
                .rooms:
            return HTTP.Method.get.rawValue
        }
    }
}
