//
//  ImageNetworkService.swift
//  VIPER
//
//  Created by Роман Степанов on 13.01.2024.
//

import UIKit
protocol WebImageProtocol {
    func getImage(url UrlStr: String, completion: @escaping (Result<UIImage, Error>) -> Void)
}

public final class WebImageManager: WebImageProtocol{
    
    private enum Errors: Error {
        case invalidURLImage
        case invalidStateImage
    }
    
    static let shared = WebImageManager()
    private let urlSession = URLSession(configuration: .ephemeral)
    
    private init() { }
    
    public func getImage(url UrlStr: String, completion: @escaping (Result<UIImage, Error>) -> Void) {
       guard let url = URL(string: UrlStr) else {
           completion(.failure(Errors.invalidURLImage))
           return
       }
       urlSession.dataTask(with: url) { data, response, error in
           DispatchQueue.main.async {
               switch(data, error) {
               case let (.some(data), nil ):
                   guard let reservation = UIImage(data: data) else { return }
                   completion(.success(reservation))
               case let (nil, .some(error)):
                   completion(.failure(error))
               default:
                   completion(.failure(Errors.invalidStateImage))
               }
           }
       }.resume()
   }
}

