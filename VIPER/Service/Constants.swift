//
//  Constants.swift
//  VIPER
//
//  Created by Роман Степанов on 05.01.2024.
//

import Foundation

public struct Constants {
    
    //MARK: API
    static let scheme = "https"
    static let baseURL = "run.mocky.io"
}
