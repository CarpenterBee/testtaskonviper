//
//  NetworkService.swift
//  VIPER
//
//  Created by Роман Степанов on 25.12.2023.
//

import Foundation

protocol NetworkServiceProtocol {
    func getRequest<T: Codable>(with request: URLRequest,
                                model: T.Type,
                                completion: @escaping (Result<T, Error>) -> Void)
}

public final class NetworkService: NetworkServiceProtocol {
    
    enum Errors: Error {
        case invalidURL
        case invalidState
    }
    
    static let shared = NetworkService()
    
    private let urlSession = URLSession(configuration: .ephemeral)
    private let jsonDecoder = JSONDecoder()
    
    private init() { }
    
    public func getRequest<T: Codable>(with request: URLRequest,
                                       model: T.Type,
                                       completion: @escaping (Result<T, Error>) -> Void) {
        urlSession.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                switch(data, error) {
                case let (.some(data), nil):
                    do {
                        let reservation = try self.jsonDecoder.decode(model, from: data)
                        completion(.success(reservation))
                    } catch {
                        completion(.failure(error))
                    }
                case let (nil, .some(error)):
                    completion(.failure(error))
                default:
                    completion(.failure(Errors.invalidState))
                }
            }
        }.resume()
    }
}
