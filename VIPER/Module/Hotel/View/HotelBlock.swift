//
//  HotelBlock.swift
//  VIPER
//
//  Created by Роман Степанов on 27.01.2024.
//

import UIKit

final class HotelBlock: UIView {
    
    private let imageCarusell = ImageCarousell()
    private let ratingView = RatingStack()
    private let costInfoBlock = KingLabel()
    
    public init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func update(model: HotelBlockModel) {
        imageCarusell.updateImages(with: model.urlsImages)
        ratingView.updateRatingAndInfo(ratingUnit: model.rating,
                                        ratingText: model.ratingName,
                                        address: model.adress,
                                        name: model.name)
        costInfoBlock.updateTitles(beforeText: "от ",
                                   bigTitle: model.price,
                                   smallDescription: model.serviceName)
    }
    
    private func setupUI() {
        setupImageContener()
        setupRatingBlock()
        setupCostBlock()
        self.layer.cornerRadius = 12
        self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    private func setupImageContener() {
        self.addSubview(imageCarusell)
        imageCarusell.backgroundColor = .red
        imageCarusell.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageCarusell.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            imageCarusell.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            imageCarusell.heightAnchor.constraint(equalToConstant: 260),
            imageCarusell.topAnchor.constraint(equalTo: self.topAnchor),
        ])
    }
    
    private func setupRatingBlock() {
        self.addSubview(ratingView)
        ratingView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            ratingView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            ratingView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            ratingView.topAnchor.constraint(equalTo: imageCarusell.bottomAnchor, constant: 16),
        ])
    }
    
    private func setupCostBlock() {
        self.addSubview(costInfoBlock)
        costInfoBlock.backgroundColor = .white
        costInfoBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            costInfoBlock.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            costInfoBlock.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            costInfoBlock.topAnchor.constraint(equalTo: ratingView.bottomAnchor, constant: 16),
            costInfoBlock.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -16),
        ])
    }
}
