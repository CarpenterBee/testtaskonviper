//
//  RatingStack.swift
//  VIPER
//
//  Created by Роман Степанов on 24.01.2024.
//

import UIKit


final class RatingStack: UIView {
    
    private let rating = RatingView()
    private let title = UILabel()
    private let textButton = KingButton(style: .adressBatton)
    private let stack = UIStackView()
    
    public init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateRatingAndInfo(ratingUnit: Int,
                                    ratingText: String,
                                    address: String,
                                    name: String) {
        self.title.text = name
        let image = UIImage(name: .star)
        self.rating.updateRatingAndInfo(image: image, rating: ratingUnit, ratingName: ratingText)
        let vm = KingButtonViewModel(title: address)
        self.textButton.setupModel(viewModel: vm)
    }
    
    private func setupUI(){
        setupStack()
        setupName()
    }
    
    private func setupStack() {
        stack.spacing = 8
        stack.axis = .vertical
        addSubview(stack)
        stack.addArrangedSubview(rating)
        stack.addArrangedSubview(title)
        stack.addArrangedSubview(textButton)
        stack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: topAnchor),
            stack.bottomAnchor.constraint(equalTo: bottomAnchor),
            stack.leftAnchor.constraint(equalTo: leftAnchor),
            stack.rightAnchor.constraint(equalTo: rightAnchor),
        ])
    }
    
    private func setupName() {
        title.setContentHuggingPriority(.defaultHigh, for: .vertical)
        title.textAlignment = .left
        title.translatesAutoresizingMaskIntoConstraints = false
        title.text = "загрузка..."
        title.font = .hotelML
        title.textColor = .black
        title.backgroundColor = .white
        title.numberOfLines = 0
    }
    
}
