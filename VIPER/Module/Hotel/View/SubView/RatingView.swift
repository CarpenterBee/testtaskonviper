//
//  RatingView.swift
//  VIPER
//
//  Created by Роман Степанов on 26.01.2024.
//

import UIKit

final class RatingView: UIView {
    
    private let label = UILabel()
    private let container = UIView()
    private let leftImage = UIImageView()
    
    public init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateRatingAndInfo(image: UIImage?, rating: Int, ratingName: String) {
        self.label.text = "\(rating) \(ratingName)"
        leftImage.image = image
    }
    private func setupUI(){
        setupRatingCase()
        setupStar()
        setupRating()
    }
    
    private func setupRatingCase() {
        container.backgroundColor = .hotelDarkYellow
        container.layer.cornerRadius = 5
        container.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(container)
    }

    private func setupRating() {
        label.font = .hotelMN
        label.text = "нет данных"
        label.textColor = .hotelYellow
        
        container.addSubview(label)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            label.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            label.leftAnchor.constraint(equalTo: leftImage.rightAnchor, constant: 2),
            
        ])
        
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: label.topAnchor, constant: -5),
            container.bottomAnchor.constraint(equalTo: label.bottomAnchor, constant: 5),
            container.rightAnchor.constraint(equalTo: label.rightAnchor, constant: 10),
            container.leftAnchor.constraint(equalTo: leftImage.leftAnchor, constant: -10),
        ])
        
    }
    
    private func setupStar() {
        container.addSubview(leftImage)
        leftImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            leftImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
            leftImage.centerYAnchor.constraint(equalTo: container.centerYAnchor),
        ])
    }
    
}

