//
//  HotelViewControllerInterfaces.swift
//  VIPER
//
//  Created by Роман Степанов on 21.01.2024.
//

import Foundation

protocol HotelViewInput: AnyObject {
    func updateHotelBlocks(hotel: Hotel)
}

protocol HotelViewOutput: AnyObject {
    func loadModel()
    
    func viewDidLoad()
    
    func showRooms()
}
