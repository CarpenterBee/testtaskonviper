//
//  ViewController.swift
//  VIPER
//
//  Created by Роман Степанов on 22.12.2023.
//

import UIKit

final class HotelViewController: UIViewController {
    
    var output: HotelViewOutput?
    
    private let scroll = UIScrollView()
    private let hotelBlock = HotelBlock()
    private let bottomBlock = BottomBlock()
    private let hotelInfoBlock = HotelInfoBlock()
    
    public init(output: HotelViewOutput? = nil) {
        self.output = output
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Oтель"
    }
    
    private func setupUI() {
        setupScrollView()
        setupHotelBlock()
        setupHotelInfoBlock()
        setupBottomBlock()
        configureBottomBlock()
    }
    
    
    private func setupScrollView() {
        scroll.showsVerticalScrollIndicator = false
        scroll.bounces = true
        view.addSubview(scroll)
        scroll.delegate = self
        scroll.contentSize.width = UIScreen.main.bounds.width
        scroll.backgroundColor = .hotelDarkWhite
        
        scroll.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scroll.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            scroll.leftAnchor.constraint(equalTo: view.leftAnchor),
            scroll.rightAnchor.constraint(equalTo: view.rightAnchor),
            scroll.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),

        ])
    }
    
    private func setupHotelBlock() {
        hotelBlock.backgroundColor = .white
        scroll.addSubview(hotelBlock)
        
        hotelBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            hotelBlock.leftAnchor.constraint(equalTo: scroll.leftAnchor),
            hotelBlock.rightAnchor.constraint(equalTo: scroll.rightAnchor),
            hotelBlock.topAnchor.constraint(equalTo: scroll.topAnchor),
            hotelBlock.widthAnchor.constraint(equalToConstant: scroll.contentSize.width)
        ])
    }
    
    private func setupHotelInfoBlock() {
        hotelInfoBlock.backgroundColor = .white
        scroll.addSubview(hotelInfoBlock)
        
        hotelInfoBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            hotelInfoBlock.leftAnchor.constraint(equalTo: scroll.leftAnchor),
            hotelInfoBlock.rightAnchor.constraint(equalTo: scroll.rightAnchor),
            hotelInfoBlock.topAnchor.constraint(equalTo: hotelBlock.bottomAnchor, constant: 8),
            hotelInfoBlock.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width)
        ])
    }
    
    private func setupBottomBlock() {
        scroll.addSubview(bottomBlock)
        
        bottomBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            bottomBlock.leftAnchor.constraint(equalTo: scroll.leftAnchor),
            bottomBlock.rightAnchor.constraint(equalTo: scroll.rightAnchor),
            bottomBlock.bottomAnchor.constraint(equalTo: scroll.bottomAnchor, constant: 28),
            bottomBlock.topAnchor.constraint(equalTo: hotelInfoBlock.bottomAnchor, constant: 12)
        ])
    }
    
    private func configureBottomBlock() {
        bottomBlock.setupModel(viewModel: .init(text: "к выбору номера", onTap: output?.showRooms))
    }

}

extension HotelViewController: HotelViewInput {
    func updateHotelBlocks(hotel: Hotel){
        hotelBlock.update(model: HotelBlockModel(
            urlsImages: hotel.imageUrls,
            rating: hotel.rating,
            ratingName: hotel.ratingName,
            adress: hotel.adress,
            name: hotel.name,
            price: hotel.minimalPrice,
            serviceName: hotel.priceForIt)
        )
        hotelInfoBlock.update(model: HotelInfoBlockModel(
            hotelDescriphon: hotel.aboutTheHotel.description,
            conveniences: hotel.aboutTheHotel.peculiarities)
        )
    }
}

extension HotelViewController: UIScrollViewDelegate { }
