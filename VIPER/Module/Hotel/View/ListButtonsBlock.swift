//
//  ListButtonsBlock.swift
//  VIPER
//
//  Created by Роман Хоменко on 27.01.2024.
//

import UIKit

final class ListButtonsBlock: UIView {
    
    private let stack = UIStackView()
    
    public init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupButtonModels(_ buttonModels: [ListButtonModel]) {
        for (index, listButtonModel) in buttonModels.enumerated() {
            let button = ListButton()
            button.setupModel(listButtonModel)
            stack.addArrangedSubview(button)
            if index != buttonModels.count - 1 {
                stack.addArrangedSubview(Separator())
            }
        }
    }
    
    private func setupUI() {
        layer.cornerRadius = 15
        backgroundColor = .hotelLightGray
        setupStack()
    }
    
    private func setupStack() {
        stack.axis = .vertical
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            stack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            stack.leftAnchor.constraint(equalTo: leftAnchor, constant: 15),
            stack.rightAnchor.constraint(equalTo: rightAnchor, constant: -15),
        ])
    }
}
