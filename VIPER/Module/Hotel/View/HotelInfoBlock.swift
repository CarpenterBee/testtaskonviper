//
//  HotelInfoBlock.swift
//  VIPER
//
//  Created by Роман Степанов on 30.01.2024.
//

import UIKit

final class HotelInfoBlock: UIView {
    
    private let brickCollection = BrickCollectionView()
    private let title = UILabel()
    private let hotelDescription = UILabel()
    private let listButtonsBlock = ListButtonsBlock()
    
    public init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func update(model: HotelInfoBlockModel) {
        brickCollection.update(texts: model.conveniences)
        hotelDescription.text = model.hotelDescriphon
    }
    
    private func setupUI() {
        self.layer.cornerRadius = 12
        setupTitle()
        setupBrickCollection()
        setupDescription()
        setupBattonBlock()
        configureListButtonsBlock()
    }
    
    private func setupTitle() {
        self.addSubview(title)
        title.setContentHuggingPriority(.defaultHigh, for: .vertical)
        title.textAlignment = .left
        title.text = "Об отеле"
        title.font = .hotelML
        title.textColor = .black
        
        title.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            title.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            title.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            title.topAnchor.constraint(equalTo: self.topAnchor,constant: 16)
        ])
    }
    
    private func setupBrickCollection() {
        self.addSubview(brickCollection)
        brickCollection.backgroundColor = .black
        
        brickCollection.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                brickCollection.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
                brickCollection.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
                brickCollection.topAnchor.constraint(equalTo: title.bottomAnchor,constant: 12),
        ])
    }
    
    private func setupDescription() {
        self.addSubview(hotelDescription)
        hotelDescription.setContentHuggingPriority(.defaultHigh, for: .vertical)
        hotelDescription.textAlignment = .left
        hotelDescription.numberOfLines = 0
        hotelDescription.font = .hotelMN
        hotelDescription.textColor = .black
        
        hotelDescription.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            hotelDescription.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            hotelDescription.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            hotelDescription.topAnchor.constraint(equalTo: brickCollection.bottomAnchor,constant: 16),
        ])
    }
        
    private func setupBattonBlock() {
        self.addSubview(listButtonsBlock)
        
        listButtonsBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            listButtonsBlock.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            listButtonsBlock.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            listButtonsBlock.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16),
            listButtonsBlock.topAnchor.constraint(equalTo: hotelDescription.bottomAnchor,constant: 16)
        ])
    }
    
    private func configureListButtonsBlock() {
        listButtonsBlock.setupButtonModels([
            ListButtonModel(title: "Удобства",
                            subtitle: "самое необходимое",
                            leftImageName: .emojiHappy,
                            rightImageName: .rightArrow),
            ListButtonModel(title: "Что включено",
                            subtitle: "самое необходимое",
                            leftImageName: .tickSquare,
                            rightImageName: .rightArrow),
            ListButtonModel(title: "Что не включено",
                            subtitle: "самое необходимое",
                            leftImageName: .closeSquare,
                            rightImageName: .rightArrow),
        ])
    }
    
}
