//
//  HotelBlockModel.swift
//  VIPER
//
//  Created by Роман Степанов on 27.01.2024.
//

import Foundation

public struct HotelBlockModel {
    let urlsImages: [String]
    let rating: Int
    let ratingName: String
    let adress: String
    let name: String
    let price: Int
    let serviceName: String
}

