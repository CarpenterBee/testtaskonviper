//
//  HotelInfoBlockModel.swift
//  VIPER
//
//  Created by Роман Степанов on 31.01.2024.
//

import Foundation

public struct HotelInfoBlockModel {
    let hotelDescriphon: String
    let conveniences: [String]
}
