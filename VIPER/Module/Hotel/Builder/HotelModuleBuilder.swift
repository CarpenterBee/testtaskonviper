//
//  WelcomeModuleBuilder.swift
//  VIPER
//
//  Created by Роман Степанов on 23.12.2023.
//

import UIKit

public final class HotelModuleBuilder {

    static func build() -> HotelViewController {
        let interactor = HotelInteractor()
        let router = HotelRouter()
        let presenter = HotelPresenter(interactor: interactor, router: router)
        let viewController = HotelViewController(output: presenter)
        viewController.view.backgroundColor = .white
        presenter.view = viewController
        return viewController
    }
}
