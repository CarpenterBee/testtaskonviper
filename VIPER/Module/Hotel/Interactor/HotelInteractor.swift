//
//  WelcomeInteractor.swift
//  VIPER
//
//  Created by Роман Степанов on 23.12.2023.
//

import Foundation

protocol HotelInteractorInterface: AnyObject {
    func loadHotelModel(completion: @escaping (Hotel) -> Void)
}

final class HotelInteractor: HotelInteractorInterface {
    
    func loadHotelModel(completion: @escaping (Hotel) -> Void) {
        guard let api = Endpoint.hotel().request else { return }
        
        NetworkService.shared
            .getRequest(with: api, model: Hotel.self) { result in
                switch result {
                case let .success(hotel):
                    completion(hotel)
                case let .failure(error):
                    print(error)
                }
        }
    }
}
