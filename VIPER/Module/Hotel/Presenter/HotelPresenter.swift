//
//  WelcomePresenter.swift
//  VIPER
//
//  Created by Роман Степанов on 23.12.2023.
//

import UIKit

final class HotelPresenter {

    weak var view: HotelViewInput?
    private var router: HotelRouterProtocol
    private var interactor: HotelInteractorInterface

    private var titleRoom: String = ""
    
    init(interactor: HotelInteractorInterface, router: HotelRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
}

extension HotelPresenter: HotelViewOutput {
    func showRooms() {
        router.openRooms(title: titleRoom)
    }
    
    func viewDidLoad(){
        loadModel()
    }
    
    public func loadModel() {
        interactor.loadHotelModel { [weak self] hotel in
            guard let self else { return }
            view?.updateHotelBlocks(hotel: hotel)
            titleRoom = hotel.name
        }
    }
}


