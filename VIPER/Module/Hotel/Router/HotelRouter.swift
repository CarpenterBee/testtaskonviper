//
//  WelcomeRouter.swift
//  VIPER
//
//  Created by Роман Степанов on 23.12.2023.
//

import Foundation

protocol HotelRouterProtocol: AnyObject {
    func openRooms(title: String)
}

final class HotelRouter: HotelRouterProtocol {
    
    public func openRooms(title: String) {
        Router.shared.openScreen(.rooms(title: title))
    }
}
