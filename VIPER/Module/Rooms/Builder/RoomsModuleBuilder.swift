//
//  RoomsModuleBuilder.swift
//  VIPER
//
//  Created by Роман Степанов on 11.02.2024.
//

import UIKit

public final class RoomsModuleBuilder {
    
    static func build(hotelName: String) -> RoomsViewController {
        let interactor = RoomsInteractor()
        let router = RoomsRouter()
        let presenter = RoomsPresenter(interactor: interactor, router: router)
        let viewController = RoomsViewController(hotelName: hotelName, output: presenter)
        presenter.view = viewController
        return viewController
    }
}
