//
//  RoomsViewControllerInterfaces.swift
//  VIPER
//
//  Created by Роман Степанов on 27.02.2024.
//

import Foundation

protocol RoomsViewInput: AnyObject {
    func updateRoomsTable(rooms: Rooms)
}

protocol RoomsViewOutput: AnyObject {
    func loadModel()
    
    func viewDidLoad()
    
    func showReservation()
}
