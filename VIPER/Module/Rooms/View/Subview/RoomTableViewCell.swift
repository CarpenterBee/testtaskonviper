//
//  RoomTableViewCell.swift
//  VIPER
//
//  Created by Роман Степанов on 14.02.2024.
//

import UIKit

final class RoomTableViewCell: UITableViewCell {
    
    private let container = UIView()
    private let imageCarusell = ImageCarousell()
    private let label = UILabel()
    private let descriptionsButton = BlueButton()
    private let brickCollection = BrickCollectionView()
    private let costInfoBlock = KingLabel()
    private let buttomButton = KingButton()
    private var containerConstraint: NSLayoutConstraint!

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .hotelDarkWhite
        setupUI()
        self.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateRoomCell(viewModel: RoomCellTableViewModel){
        imageCarusell.updateImages(with: viewModel.room.imageUrls)
        costInfoBlock.updateTitles(bigTitle: viewModel.room.price,
                                   smallDescription: viewModel.room.pricePer)
        label.text = viewModel.room.name
        configureBottomButton(viewModel: viewModel)
        brickCollection.update(texts: viewModel.room.peculiarities)
        layoutIfNeeded()
 
    }
    
    private func setupUI(){
        setupContainer()
        setupImageCarusell()
        setupTitle()
        setupBrickCollection()
        setupDescriptionButton()
        setupCostInfoBlock()
        setupButtomButton()
        configureDescriptionButton()
    }
    
    private func setupContainer() {
        contentView.addSubview(container)
        container.layer.cornerRadius = 12
        container.backgroundColor = .white
        container.clipsToBounds = true
        
        container.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 8),
            container.leftAnchor.constraint(equalTo: self.contentView.leftAnchor),
            container.rightAnchor.constraint(equalTo: self.contentView.rightAnchor),
            container.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor)
        ])
    }
    
    private func setupImageCarusell() {
        container.addSubview(imageCarusell)
        
        imageCarusell.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageCarusell.topAnchor.constraint(equalTo: container.topAnchor, constant: 16),
            imageCarusell.heightAnchor.constraint(equalToConstant: 260),
            imageCarusell.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 16),
            imageCarusell.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -16),
        ])
    }
    
    private func setupTitle() {
        container.addSubview(label)
        label.font = .hotelML
        label.numberOfLines = 0
        
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: imageCarusell.bottomAnchor, constant: 8),
            label.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 16),
            label.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -16),
        ])
    }
    
    private func setupBrickCollection() {
        container.addSubview(brickCollection)
        brickCollection.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            brickCollection.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 16),
            brickCollection.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 16),
            brickCollection.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -16),
        ])
    }
    
    private func setupDescriptionButton() {
        container.addSubview(descriptionsButton)
        
        descriptionsButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            descriptionsButton.topAnchor.constraint(equalTo: brickCollection.bottomAnchor, constant: 16),
            descriptionsButton.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 16),
        ])
    }
    
    private func setupCostInfoBlock() {
        container.addSubview(costInfoBlock)
        
        costInfoBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            costInfoBlock.topAnchor.constraint(equalTo: descriptionsButton.bottomAnchor, constant: 16),
            costInfoBlock.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 16),
            costInfoBlock.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -16),
        ])
    }
    
    private func setupButtomButton() {
        container.addSubview(buttomButton)
        
        buttomButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            buttomButton.topAnchor.constraint(equalTo: costInfoBlock.bottomAnchor, constant: 16),
            buttomButton.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 16),
            buttomButton.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -16),
            buttomButton.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -16),
        ])
    }
    
    private func configureBottomButton(viewModel: RoomCellTableViewModel) {
        buttomButton.setupModel(viewModel: .init(title: "Выбрать номер",
                                                 onTap: viewModel.onTap))
    }
    
    private func configureDescriptionButton() {
        let image = UIImage(name: .rightArrowBlue)
        descriptionsButton.setupModel(viewModel: .init(title: "Подробнее о номере",
                                                       image: image))
    }
}
