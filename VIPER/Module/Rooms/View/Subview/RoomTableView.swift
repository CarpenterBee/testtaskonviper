//
//  RoomTableView.swift
//  VIPER
//
//  Created by Роман Степанов on 14.02.2024.
//

import UIKit

final class RoomTableView: UIView {
    
    private let roomTableView = UITableView()
    private var roomsDescription: [RoomCellTableViewModel] = [] {
        didSet {
           self.roomTableView.reloadData()
        }
    }
    
    public init() {
        super.init(frame: .zero)
        setupUI()
        roomTableView.register(RoomTableViewCell.self,
                               forCellReuseIdentifier: String(describing: RoomTableViewCell.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
     public func updateRooms(_ rooms: [RoomCellTableViewModel]) {
         self.roomsDescription = rooms
    }
    
    private func setupUI() {
        setupRoomTableView()
    }
    
    private func setupRoomTableView() {
        self.addSubview(roomTableView)
        roomTableView.delegate = self
        roomTableView.dataSource = self
        roomTableView.backgroundColor = .hotelDarkWhite
        roomTableView.separatorStyle = .none
        roomTableView.clipsToBounds = true
        
        roomTableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            roomTableView.topAnchor.constraint(equalTo: self.topAnchor),
            roomTableView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            roomTableView.leftAnchor.constraint(equalTo: self.leftAnchor),
            roomTableView.rightAnchor.constraint(equalTo: self.rightAnchor),
        ])
    }
}
    
extension RoomTableView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        roomsDescription.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView
            .dequeueReusableCell(withIdentifier: String(describing: RoomTableViewCell.self),
                                 for: indexPath) as? RoomTableViewCell else {
            return UITableViewCell()
        }
        
        let roomDescription = roomsDescription[indexPath.item]
        cell.updateRoomCell(viewModel: .init(room: roomDescription.room,
                                             onTap: roomDescription.onTap))
        cell.selectionStyle = .none
        
        return cell
        
    }

}

