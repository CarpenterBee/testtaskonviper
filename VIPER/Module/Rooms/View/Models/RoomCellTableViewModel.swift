//
//  RoomCellTableViewModel.swift
//  VIPER
//
//  Created by Роман Степанов on 28.02.2024.
//

import Foundation

struct RoomCellTableViewModel {
    public let room: Room
    public let onTap: TapClosure

    public init(room: Room, onTap: @escaping TapClosure) {
        self.room = room
        self.onTap = onTap
    }
}

