//
//  RoomsViewController.swift
//  VIPER
//
//  Created by Роман Степанов on 11.02.2024.
//

import UIKit

final class RoomsViewController: UIViewController {
    
    var output: RoomsViewOutput?
    
    private let roomTableView = RoomTableView()

    private var hotelName: String?
    
    public init(hotelName: String? = "", output: RoomsViewOutput? = nil) {
        self.hotelName = hotelName
        self.output = output
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = hotelName
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidLoad()
        self.view.backgroundColor = .white
        setupUI()
    }
    
    private func setupUI() {
        setupRoomTableView()
    }
    
    private func setupRoomTableView() {
        view.addSubview(roomTableView)
    
        roomTableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            roomTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            roomTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            roomTableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            roomTableView.rightAnchor.constraint(equalTo: view.rightAnchor),
        ])
    }
    
}

extension RoomsViewController: RoomsViewInput {
    public func updateRoomsTable(rooms: Rooms) {
        let viewModel = rooms.rooms.map { room in
            return RoomCellTableViewModel(room: room,
                                          onTap: { [weak self] in
                self?.output?.showReservation()
            })
        }
        roomTableView.updateRooms(viewModel)
    }
}

