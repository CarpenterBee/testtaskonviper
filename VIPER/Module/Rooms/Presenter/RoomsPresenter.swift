//
//  RoomsPresenter.swift
//  VIPER
//
//  Created by Роман Степанов on 11.02.2024.
//

import Foundation

final class RoomsPresenter {
    
    weak var view: RoomsViewInput?
    private var router: RoomsRouterProtocol
    private var interactor: RoomsInteractorInterface
    
    init(interactor: RoomsInteractorInterface, router: RoomsRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
}

extension RoomsPresenter: RoomsViewOutput {
    func showReservation() {
        router.openReservationScreen()
    }
    
    func viewDidLoad() {
        loadModel()
    }
    
    func loadModel() {
        interactor.loadRoomsModel { [weak self] rooms in
            guard let self else { return }
            view?.updateRoomsTable(rooms: rooms)
        }
    }
}


