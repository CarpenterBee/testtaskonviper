//
//  RoomsInteractor.swift
//  VIPER
//
//  Created by Роман Степанов on 11.02.2024.
//

protocol RoomsInteractorInterface: AnyObject {
    func loadRoomsModel(completion: @escaping (Rooms) -> Void)
}

final class RoomsInteractor: RoomsInteractorInterface {
    
    func loadRoomsModel(completion: @escaping (Rooms) -> Void) {
        guard let api = Endpoint.rooms().request else { return }
        
        NetworkService.shared
            .getRequest(with: api, model: Rooms.self) { result in
                switch result {
                case let .success(rooms):
                    completion(rooms)
                case let .failure(error):
                    print(error)
                }
        }
    }
}
