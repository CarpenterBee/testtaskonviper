//
//  RoomsRouter.swift
//  VIPER
//
//  Created by Роман Степанов on 11.02.2024.
//

import Foundation

protocol RoomsRouterProtocol: AnyObject {
    func openReservationScreen()
}

final class RoomsRouter: RoomsRouterProtocol {
    
    func openReservationScreen() {
        Router.shared.openScreen(.reservation)
    }
}
