//
//  ReservationRatingBlockModel.swift
//  VIPER
//
//  Created by Роман Степанов on 29.02.2024.
//

import Foundation

public struct ReservationRatingBlockModel {
    let ratingUnit: Int
    let ratingText: String
    let address: String
    let name: String
}
