//
//  ReservationPriceBlockModel.swift
//  VIPER
//
//  Created by Роман Степанов on 01.03.2024.
//

import Foundation

public struct ReservationPriceBlockModel {
    let tourPrice: Int
    let fuelPeice: Int
    let servicePrice: Int
}
