//
//  ReservationInfoBlockModel.swift
//  VIPER
//
//  Created by Роман Степанов on 01.03.2024.
//

import Foundation

public struct ReservationInfoBlockModel {
    let departure: String
    let arrivalCountry: String
    let startDate: String
    let stopDate: String
    let numberOfNights: Int
    let hotelName: String
    let nutrition: String
    let room: String
}
