//
//  ReservationInteractor.swift
//  VIPER
//
//  Created by Роман Степанов on 29.02.2024.
//

import Foundation

protocol ReservationInteractorInterface: AnyObject {
    func loadReservationModel(completion: @escaping (Reservation) -> Void)
}

final class ReservationInteractor: ReservationInteractorInterface {
    
    func loadReservationModel(completion: @escaping (Reservation) -> Void) {
        guard let api = Endpoint.reservation().request else { return }
        
        NetworkService.shared
            .getRequest(with: api, model: Reservation.self) { result in
                switch result {
                case let .success(reservation):
                    completion(reservation)
                case let .failure(error):
                    print(error)
                }
        }
    }
}
