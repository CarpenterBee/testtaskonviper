//
//  ReservationModuleBuilder.swift
//  VIPER
//
//  Created by Роман Степанов on 29.02.2024.
//

import UIKit

public final class ReservationModuleBuilder {
    
    static func build() -> ReservationViewController {
        let interactor = ReservationInteractor()
        let router = ReservationRouter()
        let presenter = ReservationPresenter(interactor: interactor, router: router)
        let viewController = ReservationViewController(output: presenter)
        presenter.view = viewController
        return viewController
    }
}
