//
//  ReservationPresenter.swift
//  VIPER
//
//  Created by Роман Степанов on 29.02.2024.
//

import Foundation

final class ReservationPresenter {
    
    weak var view: ReservationViewInput?
    private var router: ReservationRouterProtocol
    private var interactor: ReservationInteractorInterface
    
    init(interactor: ReservationInteractorInterface, router: ReservationRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
}

extension ReservationPresenter: ReservationViewOutput {
    func showPayment() {
        router.openPaymentScreen()
    }
    
    func viewDidLoad() {
        loadModel()
    }
    
    func loadModel() {
        interactor.loadReservationModel { [weak self] reservation in
            guard let self else { return }
            view?.updateReservationInfo(reservation: reservation)
        }
    }
}
