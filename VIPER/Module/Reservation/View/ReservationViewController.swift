//
//  ReservationViewController.swift
//  VIPER
//
//  Created by Роман Степанов on 29.02.2024.
//

import UIKit

final class ReservationViewController: UIViewController {
    
    var output: ReservationViewOutput?
    
    private let scroll = UIScrollView()
    private let reservationRatingBlock = ReservationRatingBlock()
    private let reservationInfoBlock = ReservationInfoBlock()
    private let reservationPriceBlock = ReservationPriceBlock()
    private let userInfoBlock =  ReservationUserInfoBlock()
    private let userInfoCollection = UserInfoCollection()
    private let bottomBlock = BottomBlock()
    
    public init(output: ReservationViewOutput? = nil) {
        self.output = output
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidLoad()
        self.view.backgroundColor = .white
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Бронирование"
    }
    
    private func setupUI() {
        setupScrollView()
        setupContainer()
        setupContainerInfo()
        setupUserInfoBlock()
        setupUserInfoCollection()
        setupReservationPriceBlock()
        setupBottomBlock()
    }
    
    private func setupScrollView() {
        scroll.showsVerticalScrollIndicator = false
        scroll.bounces = true
        view.addSubview(scroll)
        scroll.contentSize.width = UIScreen.main.bounds.width
        scroll.backgroundColor = .hotelDarkWhite
        
        scroll.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scroll.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            scroll.leftAnchor.constraint(equalTo: view.leftAnchor),
            scroll.rightAnchor.constraint(equalTo: view.rightAnchor),
            scroll.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),

        ])
    }
    
    private func setupBottomBlock() {
        scroll.addSubview(bottomBlock)
        
        bottomBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            bottomBlock.leftAnchor.constraint(equalTo: scroll.leftAnchor),
            bottomBlock.rightAnchor.constraint(equalTo: scroll.rightAnchor),
            bottomBlock.bottomAnchor.constraint(equalTo: scroll.bottomAnchor, constant: 28),
            bottomBlock.topAnchor.constraint(equalTo: reservationPriceBlock.bottomAnchor, constant: 12),
            bottomBlock.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width)
        ])
    }
    
    private func setupContainer() {
        scroll.addSubview(reservationRatingBlock)
        reservationRatingBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            reservationRatingBlock.leftAnchor.constraint(equalTo: scroll.leftAnchor),
            reservationRatingBlock.rightAnchor.constraint(equalTo: scroll.rightAnchor),
            reservationRatingBlock.topAnchor.constraint(equalTo: scroll.topAnchor, constant: 8),
        ])
    }
    
    private func setupReservationPriceBlock() {
        scroll.addSubview(reservationPriceBlock)
        reservationPriceBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            reservationPriceBlock.leftAnchor.constraint(equalTo: scroll.leftAnchor),
            reservationPriceBlock.rightAnchor.constraint(equalTo: scroll.rightAnchor),
            reservationPriceBlock.topAnchor.constraint(equalTo: userInfoCollection.bottomAnchor, constant: 8),
        ])
    }
    
    
    private func setupContainerInfo() {
        scroll.addSubview(reservationInfoBlock)
        reservationInfoBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            reservationInfoBlock.leftAnchor.constraint(equalTo: scroll.leftAnchor),
            reservationInfoBlock.rightAnchor.constraint(equalTo: scroll.rightAnchor),
            reservationInfoBlock.topAnchor.constraint(equalTo: reservationRatingBlock.bottomAnchor, constant: 8),
        ])
    }
    
    private func setupUserInfoBlock() {
        scroll.addSubview(userInfoBlock)
        
        userInfoBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            userInfoBlock.leftAnchor.constraint(equalTo: scroll.leftAnchor),
            userInfoBlock.rightAnchor.constraint(equalTo: scroll.rightAnchor),
            userInfoBlock.topAnchor.constraint(equalTo: reservationInfoBlock.bottomAnchor, constant: 8),
        ])
    }
    
    private func setupUserInfoCollection() {
        scroll.addSubview(userInfoCollection)
        userInfoCollection.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            userInfoCollection.leftAnchor.constraint(equalTo: scroll.leftAnchor),
            userInfoCollection.rightAnchor.constraint(equalTo: scroll.rightAnchor),
            userInfoCollection.topAnchor.constraint(equalTo: userInfoBlock.bottomAnchor),
        ])
    }
    
}

extension ReservationViewController: ReservationViewInput {
    func updateReservationInfo(reservation: Reservation) {
        reservationRatingBlock.updateReservationInfo(
            model: .init(ratingUnit: reservation.horating,
                         ratingText: reservation.ratingName,
                         address: reservation.hotelAdress,
                         name: reservation.hotelName)
        )
        
        reservationInfoBlock.updateReservationInfo(
            model: .init(departure: reservation.departure,
                         arrivalCountry: reservation.arrivalCountry,
                         startDate: reservation.tourDateStart,
                         stopDate: reservation.tourDateStop,
                         numberOfNights: reservation.numberOfNights,
                         hotelName: reservation.hotelName,
                         nutrition: reservation.nutrition,
                         room: reservation.room)
        )
        
        reservationPriceBlock.updateReservationPrice(
            model: .init(tourPrice: reservation.tourPrice,
                         fuelPeice: reservation.fuelCharge,
                         servicePrice: reservation.serviceCharge)
        )
        
        let price = priceMask(number: (reservation.tourPrice + reservation.fuelCharge + reservation.serviceCharge).description)
        bottomBlock.setupModel(viewModel: .init(text: "Оплатить " + price + " ₽", onTap: output?.showPayment))
    }
                               
 }
