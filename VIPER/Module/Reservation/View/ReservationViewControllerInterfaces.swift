//
//  ReservationViewControllerInterfaces.swift
//  VIPER
//
//  Created by Роман Степанов on 29.02.2024.
//

protocol ReservationViewInput: AnyObject {
    func updateReservationInfo(reservation: Reservation)
}

protocol ReservationViewOutput: AnyObject {
    func loadModel()
    
    func viewDidLoad()
    
    func showPayment()
}

