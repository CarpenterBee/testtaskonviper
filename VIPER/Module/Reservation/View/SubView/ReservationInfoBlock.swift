//
//  ReservationInfoBlock.swift
//  VIPER
//
//  Created by Роман Степанов on 29.02.2024.
//

import UIKit

final class ReservationInfoBlock: UIView {
    
    private let summedUpBlock = SummedUpBlock()

    public init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateReservationInfo(model: ReservationInfoBlockModel){
        let summedUpModels = [SummedUpUnitModel(title: "Вылет из", info: model.departure),
                              SummedUpUnitModel(title: "Страна, город", info: model.arrivalCountry),
                              SummedUpUnitModel(title: "Даты", info: model.startDate + "-" + model.stopDate),
                              SummedUpUnitModel(title: "Кол-во ночей", info: (model.numberOfNights).description + " ночей"),
                              SummedUpUnitModel(title: "Отель", info: model.hotelName),
                              SummedUpUnitModel(title: "Номер", info: model.room),
                              SummedUpUnitModel(title: "Питание", info: model.nutrition)]
        
        summedUpBlock.setupBlockInfo(models: summedUpModels)
    }
    
    private func setupUI() {
        self.backgroundColor = .hotelWhite
        self.layer.cornerRadius = 15
        setupRatingBlock()
    }
    
    private func setupRatingBlock() {
        self.addSubview(summedUpBlock)
        summedUpBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            summedUpBlock.leftAnchor.constraint(equalTo: self.leftAnchor),
            summedUpBlock.rightAnchor.constraint(equalTo: self.rightAnchor),
            summedUpBlock.topAnchor.constraint(equalTo: self.topAnchor),
            summedUpBlock.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
}
