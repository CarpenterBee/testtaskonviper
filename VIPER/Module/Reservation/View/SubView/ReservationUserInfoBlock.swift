//
//  ReservationUserInfoBlock.swift
//  VIPER
//
//  Created by Роман Степанов on 01.03.2024.
//

import UIKit

final class ReservationUserInfoBlock: UIView {
    
    private let bigTitle = UILabel()
    private let smallTitle = UILabel()
    private let numberTextfield = HotelTextfield()
    private let mailTextfield = HotelTextfield()
    
    public init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.backgroundColor = .hotelWhite
        self.layer.cornerRadius = 15
        setupBigTitle()
        setupNumberTextfield()
        setupMailTextfield()
        setupSmallTitle()
    }
    
    private func setupBigTitle() {
        bigTitle.text = "Информация о покупателе"
        bigTitle.font = .hotelML
        self.addSubview(bigTitle)
        
        bigTitle.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            bigTitle.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            bigTitle.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            bigTitle.topAnchor.constraint(equalTo: self.topAnchor, constant: 16),
        ])
    }
    
    private func setupNumberTextfield(){
        self.addSubview(numberTextfield)
        numberTextfield.setupInitialTexts(placeholder: "Номер телефона")
        
        numberTextfield.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            numberTextfield.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            numberTextfield.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            numberTextfield.topAnchor.constraint(equalTo: bigTitle.bottomAnchor, constant: 20),
        ])
    }
    
    private func setupMailTextfield(){
        self.addSubview(mailTextfield)
        mailTextfield.setupInitialTexts(placeholder: "Почта")
        
        mailTextfield.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mailTextfield.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            mailTextfield.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            mailTextfield.topAnchor.constraint(equalTo: numberTextfield.bottomAnchor, constant: 8),
        ])
    }
    
    private func setupSmallTitle(){
        self.addSubview(smallTitle)
        smallTitle.text = "Эти данные никому не передаются. После оплаты мы вышли чек на указанный вами номер и почту"
        smallTitle.numberOfLines = 0
        smallTitle.font = .hotelRS
        smallTitle.textColor = .hotelDarkGray
        
        smallTitle.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            smallTitle.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            smallTitle.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            smallTitle.topAnchor.constraint(equalTo: mailTextfield.bottomAnchor, constant: 16),
            smallTitle.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16)
        ])
    }
}
