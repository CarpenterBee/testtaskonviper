//
//  ReservationRatingBlock.swift
//  VIPER
//
//  Created by Роман Степанов on 29.02.2024.
//

import UIKit

final class ReservationRatingBlock: UIView {
    
    private let ratingView = RatingStack()
    
    public init() {
        super.init(frame: .zero)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateReservationInfo(model: ReservationRatingBlockModel) {
        ratingView.updateRatingAndInfo(ratingUnit: model.ratingUnit,
                                       ratingText: model.ratingText,
                                       address: model.address,
                                       name: model.name)
    }
    
    private func setupUI() {
        self.backgroundColor = .hotelWhite
        self.layer.cornerRadius = 15
        setupRatingBlock()
    }
    
    private func setupRatingBlock() {
        self.addSubview(ratingView)
        ratingView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            ratingView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            ratingView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            ratingView.topAnchor.constraint(equalTo: self.topAnchor, constant: 16),
            ratingView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16)
        ])
    }
    
}
