//
//  ReservationPriceBlock.swift
//  VIPER
//
//  Created by Роман Степанов on 01.03.2024.
//

import UIKit

final class ReservationPriceBlock: UIView {
    
    private let summedUpBlock = SummedUpBlock(style: .price)
    
    public init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateReservationPrice(model: ReservationPriceBlockModel){
        let tourPrice = priceMask(number: (model.tourPrice).description)
        let fuelPeice = priceMask(number: (model.fuelPeice).description)
        let servicePrice = priceMask(number: (model.servicePrice).description)
        
        let price = priceMask(number: (model.tourPrice + model.fuelPeice + model.servicePrice).description)
        let summedUpModels = [SummedUpUnitModel(title: "Тур", info: tourPrice + " ₽"),
                            SummedUpUnitModel(title: "Топлевный сбор", info: fuelPeice + " ₽"),
                            SummedUpUnitModel(title: "Сервисный сбор", info: servicePrice + " ₽"),
                            SummedUpUnitModel(title: "К оплате", info: price + " ₽", isBoldInfo: true)]
        
        summedUpBlock.setupBlockInfo(models: summedUpModels)
    }
    
    private func setupUI() {
        self.backgroundColor = .hotelWhite
        self.layer.cornerRadius = 15
        setupSummedUpBlock()
    }
    
    private func setupSummedUpBlock() {
        self.addSubview(summedUpBlock)
        summedUpBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            summedUpBlock.leftAnchor.constraint(equalTo: self.leftAnchor),
            summedUpBlock.rightAnchor.constraint(equalTo: self.rightAnchor),
            summedUpBlock.topAnchor.constraint(equalTo: self.topAnchor),
            summedUpBlock.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
}
