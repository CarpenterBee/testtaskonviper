//
//  ReservationRouter.swift
//  VIPER
//
//  Created by Роман Степанов on 29.02.2024.
//

import Foundation

protocol ReservationRouterProtocol: AnyObject {
    func openPaymentScreen()
}

final class ReservationRouter: ReservationRouterProtocol {
    
    func openPaymentScreen() {
        Router.shared.openScreen(.payment)
    }
}
