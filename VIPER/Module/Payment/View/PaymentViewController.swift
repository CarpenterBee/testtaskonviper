//
//  PaymentViewController.swift
//  VIPER
//
//  Created by Роман Степанов on 18.03.2024.
//

import UIKit

final class PaymentViewController: UIViewController {
    
    var output: PaymentViewOutput?
    private let bottomBlock = BottomBlock()
    private let paymentBlock = PaymentBlock()
    
    public init(output: PaymentViewOutput? = nil) {
        self.output = output
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Заказ оплачен"
        setupUI()
    }
    
    private func setupUI() {
        setupPaymentBlock()
        setupBottomBlock()
        configurBottomBlock()
    }
    
    private func setupPaymentBlock() {
        view.addSubview(paymentBlock)
      
        paymentBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            paymentBlock.leftAnchor.constraint(equalTo: view.leftAnchor),
            paymentBlock.rightAnchor.constraint(equalTo: view.rightAnchor),
            paymentBlock.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            paymentBlock.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func setupBottomBlock() {
        view.addSubview(bottomBlock)
    
        bottomBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            bottomBlock.leftAnchor.constraint(equalTo: view.leftAnchor),
            bottomBlock.rightAnchor.constraint(equalTo: view.rightAnchor),
            bottomBlock.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant: -8)
        ])
    }
    
    private func configurBottomBlock() {
        bottomBlock.setupModel(viewModel: .init(text: "Cупер!",onTap: output?.СloseVC))
    }
}
