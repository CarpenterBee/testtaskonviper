//
//  PaymentBlock.swift
//  VIPER
//
//  Created by Роман Степанов on 18.03.2024.
//

import UIKit

final class PaymentBlock: UIView {
    
    private let imageConteiner = UIView()
    private let image = UIImageView()
    private let title = UILabel()
    private let paymentDiscripchon = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        setupImageConteiner()
        setupImage()
        setupTitle()
        setupPaymentDiscripchon()
    }
    
    private func getRandomNumber(min: Int, max: Int) -> Int {
        return Int.random(in: min...max)
    }
    
    private func setupImageConteiner() {
        self.addSubview(imageConteiner)
        imageConteiner.backgroundColor = .hotelDarkWhite
        imageConteiner.layer.cornerRadius = 47
        
        imageConteiner.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageConteiner.topAnchor.constraint(equalTo: self.topAnchor, constant: 122),
            imageConteiner.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            imageConteiner.heightAnchor.constraint(equalToConstant: 94),
            imageConteiner.widthAnchor.constraint(equalToConstant: 94)
        ])
    }
    
    private func setupImage() {
        imageConteiner.addSubview(image)
        image.image =  .partyPopper
        
        image.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            image.heightAnchor.constraint(equalToConstant: 44),
            image.widthAnchor.constraint(equalToConstant: 44),
            image.centerXAnchor.constraint(equalTo: imageConteiner.centerXAnchor),
            image.centerYAnchor.constraint(equalTo: imageConteiner.centerYAnchor)
        ])
    }
    
    private func setupTitle() {
        self.addSubview(title)
        title.text = "Ваш заказ принят в работу"
        title.font = .hotelML
        title.numberOfLines = 0
        title.textAlignment = .center
        
        title.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            title.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            title.topAnchor.constraint(equalTo: imageConteiner.bottomAnchor, constant: 32)
        ])
    }
    
    private func setupPaymentDiscripchon() {
        self.addSubview(paymentDiscripchon)
        let number = getRandomNumber(min: 100, max: 1000000)
        paymentDiscripchon.font = .hotelRS
        paymentDiscripchon.numberOfLines = 0
        paymentDiscripchon.textAlignment = .center
        
        paymentDiscripchon.text  =
        "Подтверждение заказа №\(number) может занять некоторое время (от 1 часа до суток). Как только мы получим ответ от туроператора, вам на почту придет уведомление."
        
        paymentDiscripchon.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            paymentDiscripchon.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            paymentDiscripchon.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 20),
            paymentDiscripchon.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -23),
            paymentDiscripchon.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 23)
        ])
    }
    
}
