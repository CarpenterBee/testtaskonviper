//
//  PaymentRouter.swift
//  VIPER
//
//  Created by Роман Степанов on 18.03.2024.
//

import Foundation

import Foundation

protocol PaymentRouterProtocol: AnyObject {
    func closeVS()
}

final class PaymentRouter: PaymentRouterProtocol {
    
    public func closeVS() {
        Router.shared.openScreen(.returnOnHotel)
    }
}
