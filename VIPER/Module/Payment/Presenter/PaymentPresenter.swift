//
//  PaymentPresenter.swift
//  VIPER
//
//  Created by Роман Степанов on 18.03.2024.
//

import Foundation

final class PaymentPresenter {

    private var router: PaymentRouterProtocol

    private var titleRoom: String = ""
    
    init(router: PaymentRouterProtocol) {
        self.router = router
    }
}

extension PaymentPresenter: PaymentViewOutput {
    
    func СloseVC() {
        router.closeVS()
    }
}
