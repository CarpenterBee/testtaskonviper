//
//  PaymentModuleBuilder.swift
//  VIPER
//
//  Created by Роман Степанов on 18.03.2024.
//

import Foundation

public final class PaymentModuleBuilder {

    static func build() -> PaymentViewController {
        let router = PaymentRouter()
        let presenter = PaymentPresenter(router: router)
        let viewController = PaymentViewController(output: presenter)
        viewController.view.backgroundColor = .white
        return viewController
    }
}
