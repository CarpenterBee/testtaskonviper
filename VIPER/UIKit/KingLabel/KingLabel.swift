//
//  KingLabel.swift
//  VIPER
//
//  Created by Роман Степанов on 26.01.2024.
//

import UIKit

final class KingLabel: UIView {
    
    private let bigTitle = UILabel()
    private let smallDescription = UILabel()
    
    public init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateTitles(beforeText: String = "" , bigTitle: Int, smallDescription: String) {
        self.smallDescription.text = smallDescription
        let price = priceMask(number: bigTitle.description)
        let text = beforeText + price + " ₽"
        self.bigTitle.text = text
    }
    
    private func setupUI() {
        setupBigTitle()
        setupSmallTitle()
    }
    
    private func setupBigTitle() {
        self.addSubview(bigTitle)
        bigTitle.textColor = .hotelBalck
        bigTitle.font = .hotelBL
        bigTitle.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            bigTitle.topAnchor.constraint(equalTo: self.topAnchor),
            bigTitle.leftAnchor.constraint(equalTo: self.leftAnchor),
            bigTitle.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    private func setupSmallTitle() {
        self.addSubview(smallDescription)
        smallDescription.textColor = .hotelDarkGray
        smallDescription.font = .hotelRS
        smallDescription.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            smallDescription.topAnchor.constraint(equalTo: self.topAnchor, constant: 14),
            smallDescription.leftAnchor.constraint(equalTo: bigTitle.rightAnchor, constant: 8),
        ])
    }
    
}
