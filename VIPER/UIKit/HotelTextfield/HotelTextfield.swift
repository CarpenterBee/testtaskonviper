//
//  HotelTextfield.swift
//  VIPER
//
//  Created by Роман Хоменко on 19.01.2024.
//

import Foundation
import CoreFoundation
import UIKit

public final class HotelTextfield: UIView {
    
    private let container = UIView()
    private let textfield = UITextField()
    private let placeholderContainer = UILabel()
    
    private lazy var textfieldBottomConstraint: NSLayoutConstraint = textfield
        .bottomAnchor
        .constraint(equalTo: container.bottomAnchor, constant: -16.0)
    
    public init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupInitialTexts(title: String? = nil, placeholder: String) {
        textfield.text = title
        placeholderContainer.text = placeholder
    }
    
    public func getText() -> String {
        textfield.text ?? ""
    }

    @objc
    public func endTextfieldEditing() {
        textfield.endEditing(true)
    }
    
    private func setupUI() {
        setupTapGesture()
        setupView()
        setupContainer()
        setupTextfield()
        setupPlaceholder()
        configureContainer()
        configureTextfield()
        configurePlaceholder()
    }
    
    private func setupTapGesture() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(setFirstResponder))
        addGestureRecognizer(gestureRecognizer)
    }
    
    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: 52)
        ])
    }
    
    private func setupContainer() {
        addSubview(container)
        container.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: topAnchor),
            container.bottomAnchor.constraint(equalTo: bottomAnchor),
            container.leftAnchor.constraint(equalTo: leftAnchor),
            container.rightAnchor.constraint(equalTo: rightAnchor),
        ])
    }
    
    private func setupTextfield() {
        container.addSubview(textfield)
        textfield.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            textfieldBottomConstraint,
            textfield.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 16.0),
            textfield.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -16.0),
        ])
    }
    
    private func setupPlaceholder() {
        container.addSubview(placeholderContainer)
        placeholderContainer.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            placeholderContainer.topAnchor.constraint(equalTo: container.topAnchor, constant: 16.0),
            placeholderContainer.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 16.0),
            placeholderContainer.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -16.0),  
        ])
    }
    
    private func configureContainer() {
        container.layer.cornerRadius = 10.0
        container.backgroundColor = .hotelDarkWhite
    }
    
    private func configureTextfield() {
        textfield.delegate = self
        textfield.font = .hotelMN
    }
    
    private func configurePlaceholder() {
        placeholderContainer.textColor = .hotelLightWhite
        placeholderContainer.font = .hotelMN
    }
    
    @objc
    private func setFirstResponder() {
        textfield.becomeFirstResponder()
    }
}

extension HotelTextfield: UITextFieldDelegate {
    public func textFieldDidChangeSelection(_ textField: UITextField) { }
    
    // TODO: - Подумай, как сделать анимацию текста получше
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == nil || textField.text == "" {
            
            self.placeholderContainer.setAnchorPoint(.zero)
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                guard let self else { return }
                self.placeholderContainer.frame.origin.y -= 6
                self.placeholderContainer.transform = CGAffineTransform(scaleX: 14/20, y: 14/20)

                self.textfieldBottomConstraint.constant = -10.0
                
                self.layoutIfNeeded()
            })
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == nil || textField.text == "" {
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                guard let self else { return }
                self.placeholderContainer.frame.origin.y += 6
                self.placeholderContainer.transform = .identity
                
                self.textfieldBottomConstraint.constant = -16.0
                
                self.layoutIfNeeded()
            })
        }
    }
}
