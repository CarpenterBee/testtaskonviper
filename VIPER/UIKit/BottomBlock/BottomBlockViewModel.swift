//
//  BottomBlockViewModel.swift
//  VIPER
//
//  Created by Роман Степанов on 07.03.2024.
//

import Foundation

public struct BottomBlockViewModel {
    public var text: String
    public var onTap: TapClosure?
    
    public init(text: String, onTap: TapClosure? = nil) {
        self.text = text
        self.onTap = onTap
    }
}
