//
//  BottomBlock.swift
//  VIPER
//
//  Created by Роман Хоменко on 15.01.2024.
//

import UIKit

public final class BottomBlock: UIView {
    
    private let stroke = UIView()
    private let button = KingButton()
    
    public init() {
        super.init(frame: .zero)
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupModel(viewModel: BottomBlockViewModel) {
        self.button.setupModel(viewModel: 
                .init(title: viewModel.text, onTap: viewModel.onTap))
    }
    
    private func setupUI() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = .hotelWhite
        self.setupStroke()
        self.setupButton()
    }
    
    private func setupStroke() {
        self.stroke.backgroundColor = .hotelStrokeGray
        self.stroke.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(stroke)
        NSLayoutConstraint.activate([
            self.stroke.heightAnchor.constraint(equalToConstant: 1.0),
            self.stroke.topAnchor.constraint(equalTo: self.topAnchor),
            self.stroke.leftAnchor.constraint(equalTo: self.leftAnchor),
            self.stroke.rightAnchor.constraint(equalTo: self.rightAnchor),
        ])
    }
    
    private func setupButton() {
        self.button.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(button)
        NSLayoutConstraint.activate([
            self.button.topAnchor.constraint(equalTo: self.topAnchor, constant: 12.0),
            self.button.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16.0),
            self.button.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16.0),
            self.button.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -28.0),
        ])
    }
}
