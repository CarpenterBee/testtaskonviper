//
//  SquareButton.swift
//  VIPER
//
//  Created by Роман Степанов on 22.03.2024.
//

import UIKit

public final class SquareButton: Control {
    
    private var rightImage = UIImageView()
    private var viewModel: SquareButtonViewModel
    
    public init(viewModel: SquareButtonViewModel = .init()) {
        self.viewModel = viewModel
        super.init()
        self.setupUI()
        self.backgroundColor = .hotelBlue
        self.layer.cornerRadius = 6
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    public override func handleTap() {
        self.viewModel.onTap?()
    }
    
    public override func updateHighlightState(with isHiglight: Bool) {
        backgroundColor = isHiglight ? .lightGray : .hotelBlue
    }
    
    public func setupModel(viewModel: SquareButtonViewModel) {
        self.viewModel = viewModel
        rightImage.image = viewModel.image
    }
    
    private func setupUI() {
        setupImage()
    }
    
    private func setupImage() {
        self.addSubview(rightImage)
        rightImage.tintColor = .hotelBlue
        rightImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            rightImage.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            rightImage.centerXAnchor.constraint(equalTo: self.centerXAnchor)

        ])
    }
    
}
