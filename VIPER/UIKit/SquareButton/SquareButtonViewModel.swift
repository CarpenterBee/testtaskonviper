//
//  SquareButtonViewModel.swift
//  VIPER
//
//  Created by Роман Степанов on 22.03.2024.
//

import UIKit

public struct SquareButtonViewModel {
    public var image: UIImage?
    public var onTap: TapClosure?
    
    public init(image: UIImage? = nil, onTap: TapClosure? = nil) {
        self.image = image
        self.onTap = onTap
    }
}
