//
//  BrickCollectionVievCell.swift
//  VIPER
//
//  Created by Роман Степанов on 31.01.2024.
//

import UIKit

final class BrickCollectionVievCell: UICollectionViewCell {
    
    private let container = UIView()
    private let label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupContainer()
        setupTitle()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateTitle(title: String){
        label.text = title
    }
    
    private func setupContainer() {
        container.backgroundColor = .hotelLightGray
        container.layer.cornerRadius = 5
        self.addSubview(container)
        
        container.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: self.topAnchor),
            container.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            container.rightAnchor.constraint(equalTo: self.rightAnchor),
            container.leftAnchor.constraint(equalTo: self.leftAnchor),
        ])
    }
    
    private func setupTitle() {
        label.textAlignment = .left
        label.font = .hotelMN
        label.textColor = .hotelDarkGray
        
        container.addSubview(label)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: container.topAnchor, constant: 5),
            label.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -5),
            label.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 10),
            label.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -10),
        ])
    }
}

