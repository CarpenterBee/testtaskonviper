//
//  BrickCollectionView.swift
//  VIPER
//
//  Created by Роман Степанов on 31.01.2024.
//

import UIKit

final class BrickCollectionView: UIView {

    private var collectionView = DynamicHeightCollectionView(frame: .zero,
                                                             collectionViewLayout: .brickCollectionLayout)
    private var discription: [String] = [] {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    public init() {
        super.init(frame: .zero)
        collectionView.register(BrickCollectionVievCell.self,
                                forCellWithReuseIdentifier: String(describing: BrickCollectionVievCell.self))
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func update(texts: [String]) {
        self.discription = texts
    }
    
    private func setupUI() {
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        self.addSubview(collectionView)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.contentInset = .zero
        collectionView.isScrollEnabled = false
        collectionView.dataSource = self
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            collectionView.leftAnchor.constraint(equalTo: self.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: self.rightAnchor),
            collectionView.topAnchor.constraint(equalTo: self.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }
    
}

extension BrickCollectionView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return discription.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: String(describing: BrickCollectionVievCell.self),
                                 for: indexPath) as? BrickCollectionVievCell else {
            return UICollectionViewCell()
        }
        
        let convinience = discription[indexPath.item]
        cell.updateTitle(title: convinience)
        
        return cell
    }
    
}

