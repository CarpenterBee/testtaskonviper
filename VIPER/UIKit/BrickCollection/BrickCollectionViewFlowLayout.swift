//
//  BrickCollectionViewFlowLayout.swift
//  VIPER
//
//  Created by Роман Степанов on 01.02.2024.
//

import UIKit

public final class BrickCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var newAttributesArray = [UICollectionViewLayoutAttributes]()
        guard let superAttributesArray = super.layoutAttributesForElements(in: rect) else {
            return []
        }
        for (index, attributes) in superAttributesArray.enumerated() {
            if index == 0 
                ||
                superAttributesArray[index - 1].frame.origin.y
                != attributes.frame.origin.y {
                attributes.frame.origin.x = sectionInset.left
            } else {
                let previousAttributes = superAttributesArray[index - 1]
                let previousFrameRight = previousAttributes.frame.origin.x + previousAttributes.frame.width
                attributes.frame.origin.x = previousFrameRight + minimumInteritemSpacing
            }
            newAttributesArray.append(attributes)
        }
        return newAttributesArray
    }
}

public extension UICollectionViewLayout {
    static var brickCollectionLayout: UICollectionViewFlowLayout {
        let layout = BrickCollectionViewFlowLayout()
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        return layout
    }
}

