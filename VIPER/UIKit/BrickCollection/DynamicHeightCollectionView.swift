//
//  DynamicHeightCollectionView.swift
//  VIPER
//
//  Created by Роман Хоменко on 03.02.2024.
//

import Foundation
import UIKit

public class DynamicHeightCollectionView: UICollectionView {
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        if !CGSizeEqualToSize(bounds.size, self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    public override var intrinsicContentSize: CGSize {
        return contentSize
    }
}
