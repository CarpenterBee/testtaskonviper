//
//  SummedUpBlock.swift
//  VIPER
//
//  Created by Роман Хоменко on 20.01.2024.
//

import Foundation
import UIKit

public final class SummedUpBlock: UIView {
    
    public enum BlockStyle {
        case price
        case location
    }
    
    private let mainStack = UIStackView()
    
    private let style: BlockStyle
    
    public init(style: BlockStyle = .location) {
        self.style = style
        super.init(frame: .zero)
        setupUI()
        mainStack.axis = .vertical
        mainStack.spacing = 16
        mainStack.distribution = .fill
        backgroundColor = .white
        layer.cornerRadius = 12
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupBlockInfo(models: [SummedUpUnitModel]) {
        for model in models {
            addUnitBlock(model: model)
        }
    }
    
    private func addUnitBlock(model: SummedUpUnitModel) {
        
        let title = UILabel()
        title.text = model.title
        title.font = .hotelMN
        title.textColor = .hotelDarkGray
        title.textAlignment = .left
        title.widthAnchor.constraint(equalToConstant: 140).isActive = style == .location ? true : false
        
        let info = UILabel()
        info.text = model.info
        info.font = .hotelRS
        let textColor: UIColor = .hotelBlue
        info.textColor = model.isBoldInfo ? textColor : .black
        
        
        info.textAlignment = style == .location ? .left : .right
        info.numberOfLines = 0
        info.widthAnchor.constraint(equalToConstant: 132).isActive = style == .price ? true : false
        
        let stack = UIStackView()
        stack.axis = .horizontal
        
        stack.addArrangedSubview(title)
        stack.addArrangedSubview(info)
        
        stack.alignment = .fill
        
        mainStack.addArrangedSubview(stack)
    }
    
    private func setupUI() {
        setupStack()
    }
    
    private func setupStack() {
        addSubview(mainStack)
        mainStack.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            mainStack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            mainStack.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            mainStack.rightAnchor.constraint(equalTo: rightAnchor, constant: -16),
        ])
    }
}
