//
//  SummedUpUnitModel.swift
//  VIPER
//
//  Created by Роман Степанов on 10.03.2024.
//

import Foundation

public struct SummedUpUnitModel {
    public let title: String
    public let info: String
    public let isBoldInfo: Bool
    
    public init(title: String, info: String, isBoldInfo: Bool = false) {
        self.title = title
        self.info = info
        self.isBoldInfo = isBoldInfo
    }
}
