//
//  ImageCarousell+Style.swift
//  VIPER
//
//  Created by Роман Степанов on 20.01.2024.
//

import Foundation

public enum CellRoundingStyle {
    case leftRounding
    case rightRounding
}
