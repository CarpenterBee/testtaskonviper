//
//  ImageCarousell.swift
//  VIPER
//
//  Created by Роман Степанов on 15.01.2024.
//


import UIKit

final class ImageCarousell: UIView {
    
    private let imageContener = UIView()
    private var infoCollection: UICollectionView!
    private let pageControl = UIPageControl()
    private var imageURLs: [String] = [] { 
        didSet {
            self.infoCollection.reloadData()
            pageControl.numberOfPages = imageURLs.count
        }
    }

    public init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    public func updateImages(with urls: [String]) {
        self.imageURLs = urls
    }
       
    private func setupUI() {
        setupImageContener()
        setupCollectionView()
        setupPageControl()
    }
        
    private func setupImageContener() {
        addSubview(imageContener)
        imageContener.backgroundColor = .white
        imageContener.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageContener.leftAnchor.constraint(equalTo: leftAnchor),
            imageContener.rightAnchor.constraint(equalTo: rightAnchor),
            imageContener.bottomAnchor.constraint(equalTo: bottomAnchor),
            imageContener.topAnchor.constraint(equalTo: topAnchor)
        ])
    }
    
    private func setupCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.width) - 32, height: 260)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0

        infoCollection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        infoCollection.layer.cornerRadius = 15
        infoCollection.backgroundColor = .white
           
        addSubview(infoCollection)
        infoCollection.showsHorizontalScrollIndicator = false
        infoCollection.isPagingEnabled = true
        infoCollection.delegate = self
        infoCollection.dataSource = self
        infoCollection.register(CarousellCollectionViewCell.self,
                                forCellWithReuseIdentifier: String(describing: CarousellCollectionViewCell.self))
        
        infoCollection.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            infoCollection.leftAnchor.constraint(equalTo: imageContener.leftAnchor),
            infoCollection.rightAnchor.constraint(equalTo: imageContener.rightAnchor),
            infoCollection.topAnchor.constraint(equalTo: imageContener.topAnchor),
            infoCollection.bottomAnchor.constraint(equalTo: imageContener.bottomAnchor),
        ])
    }
        
    private func setupPageControl() {
        infoCollection.addSubview(pageControl)
        pageControl.pageIndicatorTintColor = .gray
        pageControl.backgroundColor = .white
        pageControl.layer.cornerRadius = 5
        pageControl.currentPageIndicatorTintColor = .black
        pageControl.backgroundStyle = .automatic
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pageControl.bottomAnchor.constraint(equalTo: imageContener.bottomAnchor, constant: -8),
            pageControl.centerXAnchor.constraint(equalTo: imageContener.centerXAnchor),
            ])
        pageControl.addTarget(self, action: #selector(pageDidChange(sender:)), for: .valueChanged)
        }
        
    @objc func pageDidChange(sender: UIPageControl){
        let ofsetX = infoCollection.bounds.width * CGFloat(pageControl.currentPage)
        infoCollection.setContentOffset(CGPoint(x: ofsetX, y: 0), animated: false)
            
    }
        
}


extension ImageCarousell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
       
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageURLs.count
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {

        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: String(describing: CarousellCollectionViewCell.self),
                                 for: indexPath) as? CarousellCollectionViewCell else {
            return UICollectionViewCell()
        }
         
        let imageURL = imageURLs[indexPath.item]
            
        switch imageURL {
        case imageURLs.first:
            cell.setupCell(type: .leftRounding)
        case imageURLs.last:
            cell.setupCell(type: .rightRounding)
        default:
            break
        }

        cell.updateImage(with: imageURL)
        
        return cell
    }
    
}
