//
//  CustomCollectionViewCell.swift
//  VIPER
//
//  Created by Роман Степанов on 11.01.2024.
//


import UIKit

final class CarousellCollectionViewCell: UICollectionViewCell {
    
    private let cellContener = UIStackView()
    private let cellimage = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupImage()
        setupContener()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupCell(type: CellRoundingStyle) {
        contentView.layer.masksToBounds = true
        contentView.layer.cornerRadius = 15
        switch type{
        case .leftRounding:
            contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        case .rightRounding:
            contentView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        }
    }
    
    public func updateImage(with url: String) {
        WebImageManager.shared.getImage(url: url) { result in
            switch result {
            case let .success(success):
                DispatchQueue.main.async {
                    self.cellimage.image = success
                }
            case let .failure(error):
               print(error)
            }
        }
    }
    
    private func setupContener() {
        contentView.addSubview(cellContener)
        cellContener.backgroundColor = .darkGray
        cellContener.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cellContener.topAnchor.constraint(equalTo: contentView.topAnchor),
            cellContener.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            cellContener.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            cellContener.rightAnchor.constraint(equalTo: contentView.rightAnchor),
        ])
    }
    
    private func setupImage() {
        cellContener.addSubview(cellimage)
        cellimage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cellimage.topAnchor.constraint(equalTo: cellContener.topAnchor),
            cellimage.bottomAnchor.constraint(equalTo: cellContener.bottomAnchor),
            cellimage.leftAnchor.constraint(equalTo: cellContener.leftAnchor),
            cellimage.rightAnchor.constraint(equalTo: cellContener.rightAnchor),
        ])
        
    }
    
    private func getImag(url: String ,completion: @escaping (UIImage) -> Void) {
        
        guard let url = URL(string: url) else { return }
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let image = UIImage(data: data)
            completion(image!)
        }
        
        task.resume()
    }
}

