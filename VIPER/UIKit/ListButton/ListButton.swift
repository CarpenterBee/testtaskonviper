//
//  ListButton.swift
//  VIPER
//
//  Created by Роман Хоменко on 27.01.2024.
//

import UIKit

public final class ListButton: Control {
    
    private let leftImage = UIImageView()
    private let title = UILabel()
    private let subtitle = UILabel()
    private let rightImage = UIImageView()
    
    private let labelsStack = UIStackView()
    
    private let separator = Separator()
    
    private var onTap: TapClosure?
    
    public override init() {
        super.init()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupModel(_ viewModel: ListButtonModel) {
        onTap = viewModel.onTap
        title.text = viewModel.title
        subtitle.text = viewModel.subtitle
        leftImage.image = UIImage(name: viewModel.leftImageName)
        rightImage.image = UIImage(name: viewModel.rightImageName)
    }
    
    public override func handleTap() {
        onTap?()
    }
    
    public override func updateHighlightState(with isHiglight: Bool) { 
        backgroundColor = isHiglight
        ? .systemGray6
        : .hotelLightGray
    }
    
    private func setupUI() {
        backgroundColor = .hotelLightGray
        setupLeftImage()
        setupLabelsStack()
        setupTitle()
        setupSubtitle()
        setupRightImage()
    }
    
    private func setupLeftImage() { 
        addSubview(leftImage)
        leftImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            leftImage.leftAnchor.constraint(equalTo: leftAnchor),
        ])
    }
    
    private func setupLabelsStack() {
        labelsStack.axis = .vertical
        labelsStack.spacing = 2
        labelsStack.isUserInteractionEnabled = false

        addSubview(labelsStack)
        labelsStack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            labelsStack.leftAnchor.constraint(equalTo: leftImage.rightAnchor, constant: 12),
            labelsStack.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            labelsStack.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -10),
            
            leftImage.centerYAnchor.constraint(equalTo: labelsStack.centerYAnchor),
        ])
    }
    
    private func setupTitle() {
        title.textColor = .hotelBlackGray
        title.font = .hotelMN
        title.textAlignment = .left
        labelsStack.addArrangedSubview(title)
    }
    
    private func setupSubtitle() {
        subtitle.textColor = .hotelDarkGray
        subtitle.font = .hotelRS
        subtitle.textAlignment = .left
        labelsStack.addArrangedSubview(subtitle)
    }
    
    private func setupRightImage() {
        addSubview(rightImage)
        rightImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            rightImage.centerYAnchor.constraint(equalTo: labelsStack.centerYAnchor),
            rightImage.rightAnchor.constraint(equalTo: rightAnchor, constant: -7),
        ])
    }
}
