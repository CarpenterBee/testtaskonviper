//
//  ListButtonModel.swift
//  VIPER
//
//  Created by Роман Степанов on 10.03.2024.
//

import Foundation

public struct ListButtonModel {
    let title: String
    let subtitle: String
    let leftImageName: ImageName
    let rightImageName: ImageName
    let onTap: TapClosure?
    
    public init(title: String,
                subtitle: String,
                leftImageName: ImageName,
                rightImageName: ImageName,
                onTap: TapClosure? = nil) {
        self.title = title
        self.subtitle = subtitle
        self.leftImageName = leftImageName
        self.rightImageName = rightImageName
        self.onTap = onTap
    }
}
