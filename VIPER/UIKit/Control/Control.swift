//
//  Control.swift
//  VIPER
//
//  Created by Роман Хоменко on 15.01.2024.
//

import UIKit

public class Control: UIControl {
    
    public override var isHighlighted: Bool {
        didSet {
            updateHighlightState(with: isHighlighted)
        }
    }
    
    public init() {
        super.init(frame: .zero)
        self.addTouchHandling()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // TODO: - на будущее
    public override func pressesBegan(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        super.pressesBegan(presses, with: event)
        if let press = presses.first, press.type == .select {
            self.sendActions(for: .touchUpInside)
        }
    }
    
    public override func pressesEnded(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        super.pressesEnded(presses, with: event)
        if let press = presses.first, press.type == .select {
            self.sendActions(for: .touchUpInside)
        }
    }
    
    private func addTouchHandling() {
        self.addTarget(self, action: #selector(self.handleTap), for: .touchUpInside)
    }
    
    @objc
    open func handleTap() { }
    
    open func updateHighlightState(with isHiglight: Bool) { }
}
