//
//  KingButtonViewModel.swift
//  VIPER
//
//  Created by Роман Степанов on 10.03.2024.
//

import Foundation

public struct KingButtonViewModel {
    public var title: String?
    public var onTap: TapClosure?
    
    public init(title: String? = nil, onTap: TapClosure? = nil) {
        self.title = title
        self.onTap = onTap
    }
}
