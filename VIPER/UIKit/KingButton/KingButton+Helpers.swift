//
//  KingButton+Helpers.swift
//  VIPER
//
//  Created by Роман Степанов on 27.01.2024.
//

import Foundation

public enum ButtonStyle {
    case adressBatton
    case kingButton
}
