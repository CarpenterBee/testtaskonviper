//
//  KingButton.swift
//  VIPER
//
//  Created by Роман Хоменко on 15.01.2024.
//

import UIKit

public typealias TapClosure = () -> Void

public final class KingButton: Control {
    
    private var viewModel: KingButtonViewModel {
        didSet {
            self.configureLabel()
        }
    }
    private let style: ButtonStyle
    
    private let title: UILabel = {
        let label = UILabel()
        // TODO: - Color to Constant
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public init(viewModel: KingButtonViewModel = .init(), style: ButtonStyle = .kingButton) {
        self.viewModel = viewModel
        self.style = style
        super.init()
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    public override func handleTap() {
        self.viewModel.onTap?()
    }
    
    public override func updateHighlightState(with isHiglight: Bool) {
        switch style {
        case .kingButton:
            backgroundColor = isHiglight ? .lightGray : .hotelBlue
        case .adressBatton:
            title.textColor = isHiglight ? .lightGray : .hotelBlue
        }
    }
    
    public func setupModel(viewModel: KingButtonViewModel) {
        self.viewModel = viewModel
    }
    
    private func setupUI() {
        self.setupLabel()
        self.configureLabel()
        self.setupStyle()
    }
    
    private func setupStyle() {
        self.translatesAutoresizingMaskIntoConstraints = false
        let height: CGFloat
        let color: UIColor
        let textFont: UIFont
        
        switch style {
        case .adressBatton:
            height = 17
            color = .hotelWhite
            title.textColor = .hotelBlue
            title.textAlignment = .left
            textFont = .hotelMS
        case .kingButton:
            height = 48
            color = .hotelBlue
            self.layer.cornerRadius = 15.0
            title.textColor = .hotelWhite
            title.textAlignment = .center
            textFont = .hotelMN
        }
        self.backgroundColor = color
        self.heightAnchor.constraint(equalToConstant: height).isActive = true
        title.font = textFont
    }
    
    private func setupLabel() {
        self.addSubview(self.title)
        NSLayoutConstraint.activate([
            self.title.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            self.title.leftAnchor.constraint(equalTo: self.leftAnchor),
            self.title.rightAnchor.constraint(equalTo: self.rightAnchor),
        ])
    }
    
    private func configureLabel() {
        if title.text != viewModel.title {
            title.text = viewModel.title
        }
    }
}
