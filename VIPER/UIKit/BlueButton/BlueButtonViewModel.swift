//
//  BlueButtonViewModel.swift
//  VIPER
//
//  Created by Роман Степанов on 10.03.2024.
//

import UIKit

public struct BlueButtonViewModel {
    public var title: String?
    public var image: UIImage?
    public var onTap: TapClosure?
    
    public init(title: String? = nil, image: UIImage? = nil, onTap: TapClosure? = nil) {
        self.title = title
        self.image = image
        self.onTap = onTap
    }
}
