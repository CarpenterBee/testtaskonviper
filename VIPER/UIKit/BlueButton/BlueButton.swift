//
//  BlueButton.swift
//  VIPER
//
//  Created by Роман Степанов on 20.02.2024.
//

import UIKit

public final class BlueButton: Control {
    
    private var rightImage = UIImageView()
    private var title = UILabel()
    private var viewModel: BlueButtonViewModel {
        didSet {
            self.configureLabel()
        }
    }
    
    public init(viewModel: BlueButtonViewModel = .init()) {
        self.viewModel = viewModel
        super.init()
        self.setupUI()
        self.backgroundColor = .hotelLightBlue
        self.layer.cornerRadius = 5
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    public override func handleTap() {
        self.viewModel.onTap?()
    }
    
    public override func updateHighlightState(with isHiglight: Bool) {
        backgroundColor = isHiglight ? .lightGray : .hotelLightBlue
    }
    
    public func setupModel(viewModel: BlueButtonViewModel) {
        self.viewModel = viewModel
        title.text = viewModel.title
        rightImage.image = viewModel.image
    }
    
    private func setupUI() {
        setupImage()
        setupLabel()
        configureLabel()
    }
    
    private func setupLabel() {
        self.addSubview(title)
        title.font = .hotelMN
        title.textColor = .hotelBlue
        
        title.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            title.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
            title.rightAnchor.constraint(equalTo: rightImage.leftAnchor, constant: -2),
            title.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5)
        ])
    }
    
    private func setupImage() {
        self.addSubview(rightImage)
        rightImage.tintColor = .hotelBlue
        rightImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            rightImage.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            rightImage.rightAnchor.constraint(equalTo: rightAnchor, constant: -2)

        ])
    }
    
    private func configureLabel() {
        if title.text != viewModel.title {
            title.text = viewModel.title
        }
    }    
}
