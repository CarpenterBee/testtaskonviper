//
//  JumpAvoidingFlowLAyout.swift
//  VIPER
//
//  Created by Роман Степанов on 18.03.2024.
//

import UIKit

class JumpAvoidingFlowLAyout: UICollectionViewFlowLayout {
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        guard let collectionView = collectionView else {
            return proposedContentOffset
        }
        let targetX: CGFloat = {
            let totalWidth = collectionViewContentSize.width + collectionView.contentInset.bottom
            
            if totalWidth > collectionView.bounds.size.width {
                return proposedContentOffset.x
            }
            
            return 0
        }()
        
        let targetY: CGFloat = {
            let totalHeight = collectionViewContentSize.height + collectionView.contentInset.bottom
         
            if totalHeight > collectionView.bounds.size.height {
                return proposedContentOffset.y
            }
            
            return 0
        }()
        
        return CGPoint(x: targetX, y: targetY)
    }
}
