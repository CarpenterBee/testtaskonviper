//
//  UserInfoCollection.swift.swift
//  VIPER
//
//  Created by Роман Степанов on 10.03.2024.
//

import UIKit

final class UserInfoCollection: UIView {
    
    private var userInfoCollection: DynamicHeightCollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.width), height: 0)
        layout.scrollDirection = .vertical
        return DynamicHeightCollectionView(frame: .zero, collectionViewLayout: layout)
    }()
    
    private var cellTitle: [String] = ["Первый турист","Второй турист"]
    private var cellTitleNew: [String] = ["Третий турист","Четверты турист","Пятый турист","Шестой турист","Седьмой турист","Восьмой турист","Девятый турист","Десятый турист"]
    
    public init() {
        super.init(frame: .zero)
        setupUI()
        userInfoCollection.register(UserInfoCollectionViewCell.self,
                                   forCellWithReuseIdentifier: String(describing: UserInfoCollectionViewCell.self))
        userInfoCollection.register(AddNewCollectionViewCell.self,
                                   forCellWithReuseIdentifier: String(describing: AddNewCollectionViewCell.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        setupUserInfoCollection()
        
        self.backgroundColor = .hotelDarkWhite
    }
    
    func updateCollection() {
        if cellTitleNew.count == 0 { return } else {
            self.userInfoCollection.performBatchUpdates({
                let indexPath = IndexPath(row: cellTitle.count, section: 0)
                cellTitle.append(cellTitleNew.first!)
                cellTitleNew.remove(at: 0)
                self.userInfoCollection.insertItems(at: [indexPath])
            }, completion: nil)
        }
     }
    
    private func setupUserInfoCollection() {
        self.addSubview(userInfoCollection)
        userInfoCollection.delegate = self
        userInfoCollection.dataSource = self
        userInfoCollection.allowsMultipleSelection = true
        userInfoCollection.alwaysBounceVertical = true
        userInfoCollection.backgroundColor = .hotelDarkWhite
        userInfoCollection.isScrollEnabled = false

        userInfoCollection.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            userInfoCollection.topAnchor.constraint(equalTo: self.topAnchor),
            userInfoCollection.leftAnchor.constraint(equalTo: self.leftAnchor),
            userInfoCollection.rightAnchor.constraint(equalTo: self.rightAnchor),
            userInfoCollection.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
}
    
extension UserInfoCollection: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return cellTitle.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item < cellTitle.count  {
            guard let cell = collectionView
                .dequeueReusableCell(withReuseIdentifier: String(describing: UserInfoCollectionViewCell.self),
                                     for: indexPath) as? UserInfoCollectionViewCell else {
                return UICollectionViewCell()
            }
            
            let name = cellTitle[indexPath.item]
            cell.updateTitle(title: name)
            
            return cell
        } else {
            guard let cell = collectionView
                .dequeueReusableCell(withReuseIdentifier: String(describing: AddNewCollectionViewCell.self),
                                     for: indexPath) as? AddNewCollectionViewCell else {
                return UICollectionViewCell()
            }
            cell.cellCondition(tapClosure: { [weak self] in
                self?.updateCollection()
            })
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let isSelected = collectionView.indexPathsForSelectedItems?.contains(indexPath) ?? false
        
        if indexPath.row < cellTitle.count {
            return CGSize(width: collectionView.bounds.width, height: isSelected ? 436 : 58)
        } else {
            return CGSize(width: collectionView.bounds.width, height: isSelected ? 58 : 58)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldDeselectItemAt indexPath: IndexPath) -> Bool {
        collectionView.deselectItem(at: indexPath, animated: true)
        collectionView.performBatchUpdates(nil)
        
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
        collectionView.performBatchUpdates(nil)
        
        return true
    }

}
