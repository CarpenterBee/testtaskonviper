//
//  UserInfoCollectionViewCell.swift
//  VIPER
//
//  Created by Роман Степанов on 10.03.2024.
//

import UIKit

final class UserInfoCollectionViewCell: UICollectionViewCell {
    
    private let topContainer = UIView()
    private let bottomContainer = UIView()
    private let titleName = UILabel()
    private let imageOnButton = UIImageView()
    private let tapGesture = UITapGestureRecognizer()
    private let userInfoTextBoard = UserInfoTextBoard()
   
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupUI()
    }
    
    override var isSelected: Bool {
        didSet{
            updateAppearansce()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI(){
        setupTopContainer()
        setupBottomContainer()
        setupUserInfoTextBoard()
        setupCellName()
        setupImage()
        
        self.backgroundColor = .hotelWhite
        contentView.clipsToBounds = true
        self.layer.cornerRadius = 12
    }
    
    public func updateTitle(title: String){
        titleName.text = title
    }
    
    private func setupImage() {
        topContainer.addSubview(imageOnButton)
        imageOnButton.image = UIImage(name: .bottomArrowBlue)
        imageOnButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageOnButton.rightAnchor.constraint(equalTo: topContainer.rightAnchor, constant: -16),
            imageOnButton.topAnchor.constraint(equalTo: topContainer.topAnchor, constant: 13),
            imageOnButton.heightAnchor.constraint(equalToConstant: 32),
            imageOnButton.widthAnchor.constraint(equalToConstant: 32)
        ])
    }

    private func updateAppearansce() {
        UIView.animate(withDuration: 0.3) {
            let upsideDown = CGAffineTransform(rotationAngle: .pi * -1)
            self.imageOnButton.transform = self.isSelected ? upsideDown: .identity
        }
    }
    
    private func setupTopContainer(){
        contentView.addSubview(topContainer)
        topContainer.backgroundColor = .hotelWhite
        
        topContainer.layer.cornerRadius = 12
        topContainer.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            topContainer.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            topContainer.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            topContainer.topAnchor.constraint(equalTo: contentView.topAnchor),
            topContainer.heightAnchor.constraint(equalToConstant: 58),
            topContainer.widthAnchor.constraint(equalToConstant: (UIScreen.main.bounds.width))
        ])

    }
    private func setupBottomContainer(){
        contentView.addSubview(bottomContainer)
        bottomContainer.backgroundColor = .hotelWhite
        bottomContainer.layer.cornerRadius = 12
        bottomContainer.addGestureRecognizer(tapGesture)
        
        bottomContainer.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            bottomContainer.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            bottomContainer.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            bottomContainer.topAnchor.constraint(equalTo: topContainer.bottomAnchor),
            
        ])
    }
    
    private func setupCellName() {
        titleName.text = "Первый турист"
        titleName.font = .hotelML
        topContainer.addSubview(titleName)
        titleName.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleName.leftAnchor.constraint(equalTo: topContainer.leftAnchor,constant: 16),
            titleName.topAnchor.constraint(equalTo: topContainer.topAnchor,constant: 16)
        ])
    }
    
    private func setupUserInfoTextBoard() {
        bottomContainer.addSubview(userInfoTextBoard)
        userInfoTextBoard.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            userInfoTextBoard.leftAnchor.constraint(equalTo: bottomContainer.leftAnchor),
            userInfoTextBoard.rightAnchor.constraint(equalTo: bottomContainer.rightAnchor),
            userInfoTextBoard.topAnchor.constraint(equalTo: bottomContainer.topAnchor),
            userInfoTextBoard.bottomAnchor.constraint(equalTo: bottomContainer.bottomAnchor),
            userInfoTextBoard.heightAnchor.constraint(equalToConstant: 378)
        ])
    }
}
