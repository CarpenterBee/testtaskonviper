//
//  AddNewCollectionViewCell.swift
//  VIPER
//
//  Created by Роман Степанов on 18.03.2024.
//

import Foundation
import UIKit

final class AddNewCollectionViewCell: UICollectionViewCell {
    
    private let container = UIView()
    private let title = UILabel()
    private let button = SquareButton()
 
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func cellCondition(tapClosure: @escaping TapClosure) {
        let image = UIImage(name: .addButton)
        button.setupModel(viewModel: .init(image: image, onTap: tapClosure))
    }
    
    private func setupUI() {
        setupContainer()
        setupTitle()
        setupButton()
    }
    
    private func setupContainer() {
        contentView.addSubview(container)
        container.backgroundColor = .hotelWhite
        container.layer.cornerRadius = 15
        
        container.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: self.topAnchor),
            container.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            container.leftAnchor.constraint(equalTo: self.leftAnchor),
            container.rightAnchor.constraint(equalTo: self.rightAnchor),
            container.heightAnchor.constraint(equalToConstant: 58)
        ])
    }
    
    private func setupTitle() {
        container.addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.text = "Добавить туриста"
        title.font = .hotelML
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: container.topAnchor, constant: 16),
            title.leftAnchor.constraint(equalTo: container.leftAnchor,constant: 16),
        ])
    }
    
    private func setupButton() {
        self.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            button.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -16),
            button.topAnchor.constraint(equalTo: container.topAnchor, constant: 13),
            button.heightAnchor.constraint(equalToConstant: 32),
            button.widthAnchor.constraint(equalToConstant: 32)
        ])
    }
}
