//
//  UserInfoTextBoard.swift
//  VIPER
//
//  Created by Роман Степанов on 16.03.2024.
//

import UIKit

final class UserInfoTextBoard: UIView {
    
    private let textBoard = UIStackView()
    private let textfieldTitle = ["Имя","Фамилия","Дата рождения","Гражданство","Номер загранпаспорта","Сорк действя загранпаспорта"]
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI(){
        setupTextBoard()
        addTextField()
    }
    
    private func setupTextBoard() {
        self.addSubview(textBoard)
        textBoard.spacing = 8
        textBoard.alignment = .fill
        textBoard.axis = .vertical
        
        textBoard.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textBoard.leftAnchor.constraint(equalTo: self.leftAnchor, constant:  16),
            textBoard.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            textBoard.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
            textBoard.bottomAnchor.constraint(equalTo:  self.bottomAnchor, constant: -16),
        ])
    }
    
    private func addTextField() {
        
        for (index, text) in textfieldTitle.enumerated() {
            let textfield = HotelTextfield()
            textfield.setupInitialTexts(placeholder: text)
            textBoard.addArrangedSubview(textfield)
            print("\(index)" + "\(text)")
        }
    }
    
}
