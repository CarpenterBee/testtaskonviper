//
//  UIView+Extensions.swift
//  VIPER
//
//  Created by Роман Хоменко on 21.01.2024.
//

import UIKit

extension UIView {
    public func setAnchorPoint(_ point: CGPoint) {
        var newPoint = CGPoint(x: bounds.size.width * point.x, y: bounds.size.height * point.y)
        var oldPoint = CGPoint(x: bounds.size.width * layer.anchorPoint.x, y: bounds.size.height * layer.anchorPoint.y);

        newPoint = newPoint.applying(transform)
        oldPoint = oldPoint.applying(transform)

        var position = layer.position

        position.x -= oldPoint.x
        position.x += newPoint.x

        position.y -= oldPoint.y
        position.y += newPoint.y

        self.translatesAutoresizingMaskIntoConstraints = true
        layer.position = position
        layer.anchorPoint = point
    }
}
