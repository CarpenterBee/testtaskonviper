//
//  Helpers+PriceMask.swift
//  VIPER
//
//  Created by Роман Степанов on 07.03.2024.
//

import Foundation

public func priceMask(number: String) -> String {
    let maxIndex = number.index(number.startIndex, offsetBy: number.count)
    let regRange = number.startIndex..<maxIndex
    let pattern: String
    let with: String
    switch number.count {
    case 5:
        pattern = "(\\d{2})(\\d+)"
        with = "$1 $2"
    case 6:
        pattern = "(\\d{3})(\\d+)"
        with = "$1 $2"
    case 7:
        pattern = "(\\d{1})(\\d{3})(\\d+)"
        with = "$1 $2 $3"
    default:
        pattern = "(\\d+)"
        with = "$1"
    }
    let result = number.replacingOccurrences(of: pattern,
                                             with: with,
                                             options: .regularExpression,
                                             range: regRange)
    return result
}
