//
//  UIFonts+FontExtension.swift
//  VIPER
//
//  Created by Роман Степанов on 06.02.2024.
//

import UIKit

public extension UIFont {
    
        // MARK: Weight - 400
    static let hotelRS: UIFont = UIFont(name: SFProDisplay.regular, size: 16) ?? .systemFont(ofSize: 16)
        
        // MARK: Weight - 500
        static let hotelMS: UIFont = UIFont(name: SFProDisplay.medium, size: 14) ?? .systemFont(ofSize: 14)
        static let hotelMN: UIFont = UIFont(name: SFProDisplay.medium, size: 16) ?? .systemFont(ofSize: 16)
        static let hotelML: UIFont = UIFont(name: SFProDisplay.medium, size: 22) ?? .systemFont(ofSize: 22)
        
        // MARK: Weight - 600
        static let hotelBL: UIFont = UIFont(name: SFProDisplay.semibold, size: 30) ?? .systemFont(ofSize: 30)
}

private extension UIFont {
    
    enum SFProDisplay {
        static let semiboldItalic: String = "SFProDisplay-semiboldItalic"
        static let heavyItalic: String  = "SFProDisplay-heavyItalic"
        static let lightItalic: String = "SFProDisplay-lightItalic"
        static let blackItalic: String = "SFProDisplay-blackItalic"
        static let regular: String = "SFProDisplay-regular"
        static let thinItalic: String = "SFProDisplay-thinItalic"
        static let medium: String = "SFProDisplay-medium"
        static let bold: String = "SFProDisplay-Bold"
        static let ultralightItalic: String = " SFProDisplay-ultralightItalic"
        static let semibold: String = "SFProDisplay-semibold"
    }
}



