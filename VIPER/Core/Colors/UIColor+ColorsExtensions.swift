//
//  UIColor+ColorsExtensions.swift
//  VIPER
//
//  Created by Роман Хоменко on 27.01.2024.
//

import UIKit

public extension UIColor {
    
    static let hotelYellow = UIColor(
        red: 255/255,
        green: 168/255,
        blue: 0/255,
        alpha: 1
    )
    
    static let hotelDarkYellow = UIColor(
        red: 255/255,
        green: 199/255,
        blue: 0/255,
        alpha: 0.2
    )
    
    static let hotelBalck = UIColor(
        red: 0/255,
        green: 0/255,
        blue: 0/255,
        alpha: 1
    )
    
    static let hotelBlue = UIColor(
        red: 13/255,
        green: 114/255,
        blue: 255/255,
        alpha: 1
    )
    
    static let hotelDarkGray = UIColor(
        red: 130/255,
        green: 135/255,
        blue: 150/255,
        alpha: 1
    )
    
    static let hotelGray = UIColor(
        red: 130/255,
        green: 135/255,
        blue: 150/255,
        alpha: 0.15
    )
    
    static let hotelLightGray = UIColor(
        red: 251/255,
        green: 251/255,
        blue: 252/255,
        alpha: 1
    )
    
    static let hotelWhite = UIColor(
        red: 255/255,
        green: 255/255,
        blue: 255/255,
        alpha: 1
    )
    
    static let hotelLightBlue = UIColor(
        red: 13/255,
        green: 114/255,
        blue: 255/255,
        alpha: 0.1
    )
    
    static let hotelLightBlack = UIColor(
        red: 0/255,
        green: 0/255,
        blue: 0/255,
        alpha: 0.9
    )
    
    static let hotelBlackGray = UIColor(
        red: 44/255,
        green: 48/255,
        blue: 53/255,
        alpha: 1
    )
    
    static let hotelDarkWhite = UIColor(
        red: 246/256,
        green: 246/256,
        blue: 249/256,
        alpha: 1
    )
    
    static let hotelLightWhite = UIColor(
        red: 169/256,
        green: 171/256,
        blue: 183/256,
        alpha: 1
    )
    
    static let hotelStrokeGray = UIColor(
        red: 232/256,
        green: 233/256,
        blue: 236/256,
        alpha: 1
    )
}
