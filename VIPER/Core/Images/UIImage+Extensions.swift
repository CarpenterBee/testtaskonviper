//
//  UIImage+Extensions.swift
//  VIPER
//
//  Created by Роман Степанов on 27.02.2024.
//

import UIKit

extension UIImage {
    public convenience init?(name: ImageName) {
        self.init(named: name.rawValue)
    }
}


