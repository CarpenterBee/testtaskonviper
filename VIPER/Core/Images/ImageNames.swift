//
//  ImageNames.swift
//  VIPER
//
//  Created by Роман Хоменко on 27.01.2024.
//

public enum ImageName: String {
    case emojiHappy = "emojiHappy"
    case tickSquare = "tickSquare"
    case closeSquare = "closeSquare"
    case rightArrow = "rightArrow"
    case star = "star"
    case rightArrowBlue = "rightArrowBlue"
    case addButton = "addButton"
    case bottomArrowBlue = "bottomArrowBlue"
    case partyPopper = "partyPopper"
}
