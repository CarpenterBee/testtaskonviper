//
//  File.swift
//  VIPER
//
//  Created by Роман Степанов on 10.01.2024.
//

import UIKit

public final class Router {
    
    private var sourceNavigationController = UINavigationController()
    static let shared = Router()
    
    private init() { }
    
    public func start(with navigationController: UINavigationController, for window: UIWindow) {
        let hotelModule = HotelModuleBuilder.build()
        sourceNavigationController = navigationController
        sourceNavigationController.navigationBar.backgroundColor = .hotelWhite
        sourceNavigationController.pushViewController(hotelModule, animated: false)
        window.rootViewController = sourceNavigationController
        window.makeKeyAndVisible()
    }
    
    public func openScreen(_ route: Route) {
        switch route {
        case .hotel:
            return openHotelScreen()
        case .reservation:
            return openReservationScreen()
        case let .rooms(title):
            return openRoomsScreen(title: title)
        case .payment:
            return openPaymentScreen()
        case .returnOnHotel:
            return backToFirst()
        }
    }
    
    private func openHotelScreen() {
        let hotelModule = HotelModuleBuilder.build()
        sourceNavigationController.pushViewController(hotelModule, animated: false)
    }
    
    private func openReservationScreen() {
        let reservationModule = ReservationModuleBuilder.build()
        sourceNavigationController.pushViewController(reservationModule, animated: true)
    }
    
    private func openRoomsScreen(title: String) {
        let roomsModule = RoomsModuleBuilder.build(hotelName: title)
        sourceNavigationController.pushViewController(roomsModule, animated: true)
    }
    
    private func openPaymentScreen() {
        let paymentModule = PaymentModuleBuilder.build()
        sourceNavigationController.pushViewController(paymentModule, animated: true)
    }
    
    private func backToFirst() {
        while sourceNavigationController.viewControllers.count > 1 {
            sourceNavigationController.popViewController(animated: false)
        }
    }
    
}

