//
//  Route.swift
//  VIPER
//
//  Created by Роман Степанов on 10.01.2024.
//

import Foundation

public enum Route {
    case hotel
    case rooms(title: String)
    case reservation
    case payment
    case returnOnHotel
}
